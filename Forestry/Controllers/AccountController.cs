﻿using BusinessPOCO;
using BusinessRepository.BusinessObject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Forestry.Controllers
{
    public class AccountController : Controller
    {
        ResponseMessage responseMessage = new ResponseMessage();

        [HttpGet]
        public JsonResult IsUsernameAvailable(string userName)
        {
            try
            {
                if (!Employees.DoesUserNameExists(userName))
                {
                    return Json(responseMessage.SuccessResponse(""), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(responseMessage.UnSuccessResponse("Username already exists, please try another."), JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }

            

            
        }

        [HttpPost]
        public JsonResult Insert(Employees employeeModel)
        {
            try
            {
                Employees employee = new Employees();
                employee = employeeModel;
                employee.Id = employee.Insert();

                if (employee.Id > 0)
                {
                    return Json(responseMessage.SuccessResponse("Employee added successfully."), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(responseMessage.UnSuccessResponse("Employee not added, something went wrong."), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Authenticate(Employees employeeModel)
        {
            try
            {
                Employees employee = Employees.EmployeeAuthenticate(employeeModel);

                if (string.IsNullOrEmpty(employee.Error))
                {
                    return Json(responseMessage.SuccessResponse("Successfully logged in.", employee), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(responseMessage.UnSuccessResponse(employee.Error), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SendResetPasswordEmail(string userName)
        {
            if (Employees.DoesUserNameExists(userName))
            {
                try
                {
                    Employees employeeModel = Employees.CheckAndGenerateForgetPasswordKey(userName);

                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString());
                    mail.To.Add(employeeModel.Email);
                    mail.Subject = "Forestry - Reset password request";
                    mail.Body = "Please click <a href='"+ Request.Url.Authority + "?q=" + employeeModel.Key + "&eid="+ employeeModel.Id+ "'>here</a> to reset your password.";

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["FromEmail"].ToString(), ConfigurationManager.AppSettings["EmailPassword"].ToString());
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);
                    return Json(responseMessage.SuccessResponse("Email for change password had been sent on you provided email."), JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(responseMessage.UnSuccessResponse("Email couldnt be sent due to some problem."), JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(responseMessage.UnSuccessResponse("Username doesnt exists"), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangePassword(int empID, string password, string confirmPassword, string oldPassword, bool isForgotPassword)
        {
            try
            {
                if (!isForgotPassword)
                {
                    if (string.IsNullOrEmpty(oldPassword))
                    {
                        return Json(responseMessage.UnSuccessResponse("Please fill all required fields."), JsonRequestBehavior.AllowGet);
                    }
                }

                if (string.IsNullOrEmpty(password))
                {
                    return Json(responseMessage.UnSuccessResponse("Please fill all required fields."), JsonRequestBehavior.AllowGet);
                }
                else if (password != confirmPassword)
                {
                    return Json(responseMessage.UnSuccessResponse("Password and confirm password doesnt match."), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    bool response = Employees.ChangePassword(empID, password, oldPassword, isForgotPassword);
                    if (response)
                    {
                        return Json(responseMessage.SuccessResponse("Password changed."), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(responseMessage.UnSuccessResponse("Current password does not match."), JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult IsPageAllowed(string pageName, int? designationID, int? roleID)
        {
            try
            {
                if (pageName == null || designationID == null || roleID == null)
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (Employees.IsPageAllowed(pageName, (int)designationID, (int)roleID))
                    {
                        return Json(responseMessage.SuccessResponse(""), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult IsChangePasswordKeyValid(int? empID, string key)
        {
            try
            {
                if (empID != null && key != null)
                {
                    string response = Employees.IsChangePasswordKeyValid((int)empID, key);

                    if (string.IsNullOrEmpty(response))
                    {
                        return Json(responseMessage.SuccessResponse(response), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(responseMessage.UnSuccessResponse(response), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}