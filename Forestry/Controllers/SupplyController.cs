﻿using BusinessPOCO;
using BusinessRepository.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forestry.Controllers
{
    public class SupplyController : Controller
    {
        ResponseMessage responseMessage = new ResponseMessage();

        [HttpGet]
        public JsonResult GetByEmployee(int? id)
        {
            if(id != null)
            {
                try
                {
                    SupplyCollection supplyList = Supply.SelectByEmployee((int)id);
                    return Json(responseMessage.SuccessResponse("", (supplyList == null? new SupplyCollection() : supplyList)), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetByID(int? id)
        {
            if (id != null)
            {
                try
                {
                    Supply supplyModel = Supply.SelectByPrimaryKey((int)id);
                    if (supplyModel != null) {
                        supplyModel.supplyItemCollection = SupplyItems.GetBySupplyID(supplyModel.Id);
                        supplyModel.SupplyCoordinates = LatitudeLongitude.SelectBySupply(supplyModel.Id);
                        return Json(responseMessage.SuccessResponse("", supplyModel), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult IsItemAvailable(int? empID, int? itemID, int? itemSizeID, int? microQty, int? macroQty, int? pottedQty)
        {
            if (empID < 1 || itemID < 1 || itemSizeID < 1 || (microQty  + macroQty + pottedQty) < 1 || 
                empID == null || itemID == null || itemSizeID == null || microQty == null || macroQty == null || pottedQty == null)
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    string result = ItemInventory.IsItemAvailable((int)empID, (int)itemID, (int)itemSizeID, (int)microQty, (int)macroQty, (int)pottedQty);
                    if (string.IsNullOrEmpty(result)) {
                        return Json(responseMessage.SuccessResponse("", result), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(responseMessage.UnSuccessResponse(result), JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult InsertUpdate(Supply supplyModel, List<SupplyItems> supplyItems, List<LatitudeLongitude> supplyCoordinates)
        {
            try
            {
                if(supplyModel.Id > 0)
                {
                    supplyModel.Update();

                    if (supplyItems != null)
                    {
                        foreach (SupplyItems supplyItem in supplyItems)
                        {
                            if (supplyItem.IsNew)
                            {
                                supplyItem.SupplyID = supplyModel.Id;
                                supplyItem.Insert();
                            }
                            else
                            {
                                supplyItem.Update();
                            }
                        }
                    }
                    
                    if (supplyCoordinates != null)
                    {
                        LatitudeLongitude.DeleteBySupply(supplyModel.Id);

                        foreach (LatitudeLongitude supplyLatLng in supplyCoordinates)
                        {
                            supplyLatLng.SupplyID = supplyModel.Id;
                            supplyLatLng.Insert();
                        }
                    }

                    return Json(responseMessage.SuccessResponse("Supplies had been updated."), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    supplyModel.Id = supplyModel.Insert();

                    if (supplyItems != null)
                    {
                        foreach (SupplyItems supplyItem in supplyItems)
                        {
                            if (supplyItem.IsNew)
                            {
                                supplyItem.SupplyID = supplyModel.Id;
                                supplyItem.Insert();
                            }
                            else
                            {
                                supplyItem.Update();
                            }
                        }
                    }

                    if(supplyCoordinates != null)
                    {
                        foreach(LatitudeLongitude supplyLatLng in supplyCoordinates)
                        {
                            supplyLatLng.SupplyID = supplyModel.Id;
                            supplyLatLng.Insert();
                        }
                    }

                    return Json(responseMessage.SuccessResponse("Item had been supplied."), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Delete(int? id)
        {
            if (id != null)
            {
                try
                {
                    Supply.Delete((int)id);
                    return Json(responseMessage.SuccessResponse("Supply had been removed."), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteSupplyItem(int? id)
        {
            if (id != null)
            {
                try
                {
                    SupplyItems.Delete((int)id);
                    return Json(responseMessage.SuccessResponse("Item had been removed."), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetSupplyReport(int? id)
        {
            if (id != null)
            {
                try
                {
                    SupplyCollection supplyList = Supply.GetSupplyReport((int)id);
                    return Json(responseMessage.SuccessResponse("", (supplyList == null ? new SupplyCollection() : supplyList)), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult GetMapCoordinates(int? id)
        {
            if (id != null)
            {
                try
                {
                    LatitudeLongitudeCollection latlngList = LatitudeLongitude.GetMapCoordinates((int)id);
                    LatitudeLongitudeCollection latlngListGrouped = new LatitudeLongitudeCollection();

                    if (latlngList != null)
                    {
                        foreach (LatitudeLongitude latlng in latlngList)
                        {
                            if (latlngListGrouped.Any(x => x.SupplyID == latlng.SupplyID))
                            {
                                latlngListGrouped.Where(x => x.SupplyID == latlng.SupplyID).FirstOrDefault().LatLngArr.Add(new Decimal[] { (Decimal)latlng.Latitude, (Decimal)latlng.Longitude }); ;
                            }
                            else
                            {
                                Decimal[] Arr = new Decimal[] { (Decimal)latlng.Latitude, (Decimal)latlng.Longitude };
                                latlng.LatLngArr.Add(Arr);
                                latlngListGrouped.Add(latlng);
                            }
                        }
                    }

                    return Json(responseMessage.SuccessResponse("", (latlngListGrouped == null ? new LatitudeLongitudeCollection() : latlngListGrouped)), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(responseMessage.ErrorResponse(), JsonRequestBehavior.AllowGet);
            }
        }

    }
}