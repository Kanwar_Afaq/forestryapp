/**
 * HOMER - Responsive Admin Theme
 * version 1.8
 *
 */

function configState($stateProvider, $urlRouterProvider, $compileProvider) {

    // Optimize load start with remove binding information inside the DOM element
    $compileProvider.debugInfoEnabled(true);

    // Set default state
    $urlRouterProvider.otherwise("/login");
    $stateProvider

        // Dashboard - Main page
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "/AppViews/dashboard.html",
            controller: "dashboardCtrl",
            data: {
                pageTitle: 'Dashboard'
            }
        })

        //Employees - Employee Access Page
        .state('employees', {
            url: "/employees",
            templateUrl: "/AppViews/Employees.html",
            controller: "employeesCtrl",
            data: {
                pageTitle: 'Employees'
            }
        })

        //Employees - Assign employees under employee
        .state('assignemployees', {
            url: "/assignemployees",
            templateUrl: "/AppViews/EmployeeHierarchy.html",
            controller: "employeesHierarchyCtrl",
            data: {
                pageTitle: 'Assign Employees'
            }
        })

        //Nursery Inventory - Items Inventory Page
        .state('nurseryinventory', {
            url: "/inventory",
            templateUrl: "/AppViews/NurseryInventory.html",
            controller: "nurseryinventoryCtrl",
            data: {
                pageTitle: 'Nursery Inventory'
            }
        })

        //Scheme - Manipulate scheme
        .state('scheme', {
            url: "/scheme",
            templateUrl: "/AppViews/Scheme.html",
            controller: "schemeCtrl",
            data: {
                pageTitle: 'Scheme'
            }
        })

        //Scheme - Manipulate scheme
        .state('items', {
            url: "/plants",
            templateUrl: "/AppViews/Items.html",
            controller: "itemsCtrl",
            data: {
                pageTitle: 'Plants'
            }
        })

        //Supply - supply items
        .state('supply', {
            url: "/supply",
            templateUrl: "/AppViews/Supply.html",
            controller: "supplyCtrl",
            data: {
                pageTitle: 'Supply'
            }
        })

        //ChangePassword - Change password of current user
        .state('changepassword', {
            url: "/changepassword/:q/:eid",
            templateUrl: "/AppViews/ChangePassword.html",
            controller: "employeesCtrl",
            data: {
                pageTitle: 'Change Password'
            }
        })

        // Profile - Change personal info
        .state('profile', {
            url: "/profile",
            templateUrl: "/AppViews/Profile.html",
            controller: "employeesCtrl",
            data: {
                pageTitle: 'Profile'
            }
        })

        // Profile - Change personal info
        .state('prs', {
            url: "/prs/:id",
            templateUrl: "/AppViews/PRS.html",
            controller: "prsCtrl",
            data: {
                pageTitle: 'PRS'
            }
        })
        
        //Reports - Gets Inventory Report
        .state('inventoryreport', {
            url: "/inventoryreport",
            templateUrl: "/AppViews/InventoryReport.html",
            controller: "nurseryinventoryCtrl",
            data: {
                pageTitle: 'Inventory Report'
            }
        })

        //Reports - Gets Supply Report
        .state('supplyreport', {
            url: "/supplyreport",
            templateUrl: "/AppViews/SupplyReport.html",
            controller: "supplyCtrl",
            data: {
                pageTitle: 'Supply Report'
            }
        })

        //Map - Gets Map and Point
        .state('map', {
            url: "/map",
            templateUrl: "/AppViews/Map.html",
            controller: "mapCtrl",
            data: {
                pageTitle: 'Map'
            }
        })

        // Login - Login Page
        .state('login', {
            url: "/login",
            templateUrl: "/AppViews/Login.html",
            controller: "authenticationCtrl",
            data: {
                pageTitle: 'Login'
            }
        })

        // Registration - Employee Registration Page
        .state('register', {
            url: "/register",
            templateUrl: "/AppViews/Register.html",
            controller: "authenticationCtrl",
            data: {
                pageTitle: 'Register'
            }
        });
}

angular
    .module('homer')
    .config(configState)
    .run(function ($rootScope, $state, editableOptions, $cookies, authenticationService, $location, notify) {
        $rootScope.$state = $state;
        editableOptions.theme = 'bs3';

        $rootScope.AuthenticatedUser = $cookies.getObject('AuthenticatedUser') || null;

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (!authenticationService.IsEmployeeLoggedIn()) {
                //console.log('DENY ' + JSON.stringify(fromState) + ' - ' + JSON.stringify(toState));

                if (toState.name !== "login" && toState.name !== "register" && toState.name !== 'changepassword') {
                    event.preventDefault();
                    $state.go('login');
                }
            } else {
                if ($rootScope.AuthenticatedUser !== null && $rootScope.AuthenticatedUser !== undefined) {
                    //if ($state.includes(toState.name) !== undefined) {
                    if (toState.name !== 'dashboard' && toState.name !== "login" && toState.name !== "register") {
                            authenticationService.IsPageAllowed(toState.name, $rootScope.AuthenticatedUser.DesignationID, $rootScope.AuthenticatedUser.RoleID).then(function (response) {
                                if (!response.data.IsSuccess) {
                                    event.preventDefault();
                                    $state.go('dashboard');
                                    notify({ message: 'Sorry you are not authorized to this page, please contact admin.', classes: 'alert-danger' });
                                }
                            }, function (error) {
                                event.preventDefault();
                                $state.go('dashboard');
                                notify({ message: 'Sorry something went wrong, please reach developer.', classes: 'alert-danger' });
                            });
                        }
                    //}
                }
            }
        });
    });