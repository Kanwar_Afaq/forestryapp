﻿angular
    .module('homer')
    .controller('prsCtrl', prsCtrl);

function prsCtrl($scope, $rootScope, $state, notify, prsService, $stateParams, itemsService, raisingService, schemeService, locationService) {
    
    var PRS = {
        Id: 0,
        PlantationType: 'roadside',
        RaisingID: "0",
        Name: "",
        SupplyID: $stateParams.id === undefined ? 0 : $stateParams.id,
        District: "",
        Taluka: "",
        PlantationDate: new Date,
        Distance: 0.00,
        PlantsKm: 0,
        TotalPlants: 0,
        KMFrom: 0.00,
        KMTo: 0.00,
        RestockingDone: false,
        NoOfRestocking: "0",
        SuccessPercent: "0",
        LocationID: "0",
        SchemeID: "0",
        CreatedDate: new Date,
        CreatedBy: $rootScope.AuthenticatedUser.Id,
        ModifiedDate: new Date,
        ModifiedBy: $rootScope.AuthenticatedUser.Id,
        Active: true,
        PRSItemCollection: []
    }

    $scope.PRS = PRS;
    
    GetDdls();
    RenderPRS();
    
    $scope.CalculateDistance = function () {
        $scope.PRS.Distance = ($scope.PRS.KMTo - $scope.PRS.KMFrom) > 0 ? $scope.PRS.KMTo - $scope.PRS.KMFrom : 0.00;

        $scope.CalculateTotalPlants();
    }

    $scope.CalculateTotalPlants = function () {
        $scope.PRS.TotalPlants = $scope.PRS.Distance * $scope.PRS.PlantsKM;
    }

    $scope.InsertUpdatePRS = function () {
        var ItemCollection = $scope.PRS.PRSItemCollection;
        $scope.PRS.PRSItemCollection = [];

        if (ItemCollection !== null) {
            if (ItemCollection.length > 0) {
                for (var i = 0; i < ItemCollection.length; i++) {
                    $scope.PRS.PRSItemCollection.push({ Id: 0, prsid: 0, itemid: ItemCollection[i].Id });
                }
            }
        } 

        prsService.InsertUpdate($scope.PRS).then(function (response) {
            if (response.data.IsSuccess) {
                notify({ message: response.data.Message, classes: 'alert-success' });
                $state.go('supply');
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    

    function GetDdls() {
        itemsService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.ItemList = response.data.ResponseObject;
            } else {
                $scope.ItemList = [];
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });

        raisingService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.RaisingList = response.data.ResponseObject;
            } else {
                $scope.RaisingList = [];
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

        schemeService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.SchemeList = response.data.ResponseObject;
            } else {
                $scope.SchemeList = [];
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });

        locationService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.LocationList = response.data.ResponseObject;
            } else {
                $scope.LocationList = [];
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    function BindPRSModel(PRSModel) {
        if (PRSModel !== null) {
            $scope.PRS.Id = PRSModel.Id;
            $scope.PRS.PlantationType = PRSModel.PlantationType;
            $scope.PRS.RaisingID = String(PRSModel.RaisingID);
            $scope.PRS.Name = PRSModel.Name;
            $scope.PRS.SupplyID = PRSModel.SupplyID;
            $scope.PRS.District = PRSModel.District;
            $scope.PRS.Taluka = PRSModel.Taluka;
            $scope.PRS.PlantationDate = PRSModel.PlantationDate;
            $scope.PRS.Distance = PRSModel.Distance;
            $scope.PRS.PlantsKm = PRSModel.PlantsKM !== null? parseInt(PRSModel.PlantsKm) : 0;
            $scope.PRS.TotalPlants = PRSModel.TotalPlants;
            $scope.PRS.KMFrom = PRSModel.KMFrom;
            $scope.PRS.KMTo = PRSModel.KMTo;
            $scope.PRS.RestockingDone = PRSModel.RestockingDone;
            $scope.PRS.NoOfRestocking = String(PRSModel.NoOfRestocking);
            $scope.PRS.SuccessPercent = PRSModel.SuccessPercent;
            $scope.PRS.LocationID = String(PRSModel.LocationID);
            $scope.PRS.SchemeID = String(PRSModel.SchemeID);
            $scope.PRS.CreatedDate = PRSModel.CreatedDate;
            $scope.PRS.CreatedBy = PRSModel.CreatedBy;
            $scope.PRS.ModifiedDate = new Date;
            $scope.PRS.ModifiedBy = $rootScope.AuthenticatedUser.Id;
            $scope.PRS.Active = PRSModel.Active;

            for (var i = 0; i < PRSModel.PRSItemCollection.length; i++) {
                var item = $scope.ItemList.filter(x => x.Id === PRSModel.PRSItemCollection[i].ItemID);
                $scope.PRS.PRSItemCollection.push(item);
            }
        }
    }

    function RenderPRS() {
            prsService.GetBySupply($scope.PRS.SupplyID).then(function (response) {
                if (response.data.IsSuccess) {
                    BindPRSModel(response.data.ResponseObject);
                } else {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                }
            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
    }
}

