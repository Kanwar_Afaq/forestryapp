﻿angular
    .module('homer')
    .controller('supplyCtrl', supplyCtrl);

function supplyCtrl($scope, $rootScope, $compile, $state, $uibModal, notify, sweetAlert, supplyService, nurseryinventoryService, DTOptionsBuilder, DTColumnBuilder, itemsService, itemsizeService, raisingService, schemeService) {

    var SupplyItem = {
        Id: 0,
        SupplyID: "0",
        ItemID: "0",
        ItemSizeID: "0",
        TotalQty: 0,
        MicroQty: 0,
        MacroQty: 0,
        PottedQty: 0,
        Active: true,
        CreatedDate: new Date,
        CreatedBy: $rootScope.AuthenticatedUser.Id,
        ModifiedDate: new Date,
        ModifiedBy: $rootScope.AuthenticatedUser.Id,
        IsNew: true
    }
    var Supply = {
        Id: 0,
        EmpID: $rootScope.AuthenticatedUser.Id,
        RaisingID: "0",
        SchemeID: "0",
        CustomerID: "0",
        CustomerName: "",
        ArmedForcesID: "0",
        Contact: "",
        CNIC: "",
        SupplyDate: new Date,
        X: 0,
        Y: 0,
        Active: true,
        CreatedDate: new Date,
        CreatedBy: $rootScope.AuthenticatedUser.Id,
        ModifiedDate: new Date,
        ModifiedBy: $rootScope.AuthenticatedUser.Id,
        supplyItemCollection: [],
        SupplyCoordinates: []
    }
    var ItemCollectionFromServer = [];
    var supplyItemID = 0;

    $scope.SupplyItem = SupplyItem;
    $scope.Supply = Supply;

    $scope.IsSupplyFormValid = true;
    $scope.IsSupplyItemFormValid = true;
    $scope.supplyDTInstance = {};

    $scope.SchemeList = [];
    $scope.ItemSizeList = [];
    $scope.ItemsList = [];
    $scope.RaisingList = [];
    $scope.CustomerList = [{ Id: 1, Name: 'Armed Forces' }, { Id: 2, Name: 'Farmer' }, { Id: 3, Name: 'Customer-3' }];

    if ($state.current.name === 'supplyreport') {
        RenderSupplyReport();
    } else {
        GetItemInventoryByEmployeeSum();
        RenderSupplies();
        GetDropDowns();
    }

    $scope.IsAddSupply = false;

    $scope.AddNewSupply = function () {
        $scope.IsAddSupply = !$scope.IsAddSupply;
        ClearSupplyModel();
        ClearSupplyItemModel();

        $scope.IsSupplyFormValid = true;
        $scope.IsSupplyItemFormValid = true;
        ItemCollectionFromServer = [];
        $scope.TotalQtyError = "";
        $scope.SupplyForm.$setUntouched();
    }

    $scope.GetTotalQty = function () {
        $scope.SupplyItem.TotalQty = $scope.SupplyItem.MicroQty + $scope.SupplyItem.MacroQty + $scope.SupplyItem.PottedQty;
        if ($scope.SupplyItem.TotalQty > 0) {
            $scope.TotalQtyError = "";
        } else {
            $scope.TotalQtyError = "Total quantity must be greater than 0";
        }
    }
    $scope.FixDataOnCustomer = function () {
        if (parseInt($scope.Supply.CustomerID) !== 1 || parseInt($scope.Supply.CustomerID) !== 2) {
            $scope.Supply.CustomerName = "";
            $scope.Supply.ArmedForcedID = "0";
            $scope.Supply.Contact = "";
            $scope.Supply.CNIC = "";
        }
    }

    $scope.GetItemName = function (id) {
        return $scope.ItemsList.filter(x => x.Id === parseInt(id))[0].Name;
    }
    $scope.GetItemSize = function (id) {
        return $scope.ItemSizeList.filter(x => x.Id === parseInt(id))[0].Size;
    }

    $scope.InsertUpdateSupply = function () {
        if (SupplyFormValidate()) {
            $scope.Supply.supplyItemCollection = JSON.parse(angular.toJson($scope.Supply.supplyItemCollection))

            supplyService.InsertUpdate($scope.Supply).then(function (response) {

                if (response.data.IsSuccess) {
                    notify({ message: response.data.Message, classes: 'alert-success' });
                    ClearSupplyModel();
                    ClearSupplyItemModel();
                    GetItemInventoryByEmployeeSum();
                    $scope.IsAddSupply = false;
                } else {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                }

            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
        }
    }
    $scope.GetSupplyInfo = function (id) {
        supplyService.GetByID(id).then(function (response) {
            if (response.data.IsSuccess) {
                BindSupplyModel(response.data.ResponseObject);
                $scope.IsAddSupply = true;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }
    $scope.DeleteSupply = function (id) {

        sweetAlert.swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    supplyService.Delete(id).then(function (response) {
                        if (response.data.IsSuccess) {
                            $scope.supplyDTInstance.reloadData();
                            notify({ message: response.data.Message, classes: 'alert-success' });
                        } else {
                            notify({ message: response.data.Message, classes: 'alert-danger' });
                        }
                    }, function (error) {
                        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
                    });
                }
            });
    }

    $scope.GetCoordinates = function () {
        MapModalInstance = $uibModal.open({
            templateUrl: '/AppViews/modals/mapModal.html',
            controller: MapModalInstanceCtrl,
            scope: $scope,
            size: 'lg',
            windowClass: "hmodal-info"
        });

        $('#myModal').on('show.bs.modal', function () {
            setTimeout(function () {
                map.invalidateSize();
            }, 10);
        });
    }

    $scope.DeleteSupplyItem = function (id) {

        sweetAlert.swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    angular.forEach($scope.Supply.supplyItemCollection, function (value, key) {
                        if (value.Id === id) {
                            $scope.Supply.supplyItemCollection.splice(key, 1);

                            if (!value.IsNew) {
                                supplyService.DeleteSupplyItem(id).then(function (response) {

                                }, function (error) {
                                    notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
                                });
                            }
                        }
                    });
                }
            });
        
    }
    $scope.AddSupplyItem = function () {
        if (SupplItemFormValidate()) {

            var ItemInList = $scope.Supply.supplyItemCollection.filter(x => x.ItemID === parseInt($scope.SupplyItem.ItemID) && x.ItemSizeID === parseInt($scope.SupplyItem.ItemSizeID));
            var DummyItemInList = ItemCollectionFromServer.filter(x => x.ItemID === parseInt($scope.SupplyItem.ItemID) && x.ItemSizeID === parseInt($scope.SupplyItem.ItemSizeID));

            var microQty = ItemInList.length > 0 > 0 ? ItemInList[0].MicroQty + $scope.SupplyItem.MicroQty : $scope.SupplyItem.MicroQty;
            var macroQty = ItemInList.length > 0 > 0 ? ItemInList[0].MacroQty + $scope.SupplyItem.MacroQty : $scope.SupplyItem.MacroQty;
            var pottedQty = ItemInList.length > 0 > 0 ? ItemInList[0].PottedQty + $scope.SupplyItem.PottedQty : $scope.SupplyItem.PottedQty;

            //Decrease server side quantity
            microQty = DummyItemInList.length > 0 ? microQty - DummyItemInList[0].MicroQty : microQty;
            macroQty = DummyItemInList.length > 0 ? macroQty - DummyItemInList[0].MacroQty : macroQty;
            pottedQty = DummyItemInList.length > 0 ? pottedQty - DummyItemInList[0].PottedQty : pottedQty;

            supplyService.IsItemAvailable($scope.Supply.EmpID, $scope.SupplyItem.ItemID, $scope.SupplyItem.ItemSizeID, microQty, macroQty, pottedQty).then(function (response) {
                if (response.data.IsSuccess) {
                    ItemInList = $scope.Supply.supplyItemCollection.filter(x => x.ItemID === parseInt($scope.SupplyItem.ItemID) && x.ItemSizeID === parseInt($scope.SupplyItem.ItemSizeID));
                    if (ItemInList.length > 0) {
                        ItemInList[0].MicroQty += $scope.SupplyItem.MicroQty;
                        ItemInList[0].MacroQty += $scope.SupplyItem.MacroQty;
                        ItemInList[0].PottedQty += $scope.SupplyItem.PottedQty;
                        ItemInList[0].TotalQty += $scope.SupplyItem.TotalQty;
                    } else {
                        $scope.SupplyItem.Id = ++supplyItemID;
                        var SupplyItemCopy = angular.copy($scope.SupplyItem);
                        SupplyItemCopy.ItemID = parseInt(SupplyItemCopy.ItemID);
                        SupplyItemCopy.ItemSizeID = parseInt(SupplyItemCopy.ItemSizeID);
                        SupplyItemCopy.SupplyID = parseInt(SupplyItemCopy.SupplyID);
                        $scope.Supply.supplyItemCollection.push(SupplyItemCopy);
                    }

                    ClearSupplyItemModel();
                    $scope.SupplyForm.$setUntouched();
                } else {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                    //$scope.SupplyItemError = response.data.Message;
                }
            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
        }

    }


    function RenderSupplies() {
        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
            return supplyService.GetByEmployee($rootScope.AuthenticatedUser.Id).then(function (response) {
                return response.data.ResponseObject;
            }, function (err) {
                return null;
            });
        })
            .withDOM("<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp")
            .withOption('createdRow', function (row, data, dataIndex) {
                $compile(angular.element(row))($scope);
                row.compiled = true;
            })
            .withButtons([
                { extend: 'copy', className: 'btn-sm' },
                { extend: 'csv', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'pdf', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ]);
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('RaisingName').withTitle('Raising'),
            DTColumnBuilder.newColumn('CustomerName').withTitle('Customer'),
            DTColumnBuilder.newColumn('SchemeName').withTitle('Scheme'),
            DTColumnBuilder.newColumn('TotalQty').withTitle('No. Of Plants'),
            DTColumnBuilder.newColumn('SuppliedDate').withTitle('Supplied On'),
            DTColumnBuilder.newColumn('Id', 'Action')
                .renderWith(function (id) {
                    return "<a ng-click='GetSupplyInfo(" + id + ")'><i class='fa fa-edit' title='Edit'></i></a> | <a ng-click='DeleteSupply(" + id + ")'><i class='fa fa-trash' title='Delete'></i></a> | <a ui-sref='prs({id: "+id+"})'><i class='fa' title='PRS'>PRS</i></a>";
                }).notSortable()
        ];
    }

    function GetItemInventoryByEmployeeSum() {
        nurseryinventoryService.GetByEmployeeSum($rootScope.AuthenticatedUser.Id).then(function (response) {
            if (response.data.IsSuccess) {
                $scope.TotalItemsInventory = response.data.ResponseObject;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    function ClearSupplyModel() {
        $scope.Supply.Id = 0;
        $scope.Supply.EmpID = $rootScope.AuthenticatedUser.Id;
        $scope.Supply.RaisingID = "0";
        $scope.Supply.SchemeID = "0";
        $scope.Supply.CustomerID = "0";
        $scope.Supply.CustomerName = "";
        $scope.Supply.ArmedForcedID = "0";
        $scope.Supply.Contact = "";
        $scope.Supply.CNIC = "";
        $scope.Supply.SupplyDate = new Date;
        $scope.Supply.X = 0;
        $scope.Supply.Y = 0;
        $scope.Supply.Active = true;
        $scope.Supply.CreatedDate = new Date;
        $scope.Supply.CreatedBy = $rootScope.AuthenticatedUser.Id;
        $scope.Supply.ModifiedDate = new Date;
        $scope.Supply.ModifiedBy = $rootScope.AuthenticatedUser.Id;
        $scope.Supply.supplyItemCollection = [];
        $scope.Supply.SupplyCoordinates = [];
    }

    function ClearSupplyItemModel() {
        $scope.SupplyItem.Id = 0;
        $scope.SupplyItem.SupplyID = "0";
        $scope.SupplyItem.ItemID = "0";
        $scope.SupplyItem.ItemSizeID = "0";
        $scope.SupplyItem.TotalQty = 0;
        $scope.SupplyItem.MicroQty = 0;
        $scope.SupplyItem.MacroQty = 0;
        $scope.SupplyItem.PottedQty = 0;
        $scope.SupplyItem.Active = true;
        $scope.SupplyItem.CreatedDate = new Date;
        $scope.SupplyItem.CreatedBy = $rootScope.AuthenticatedUser.Id;
        $scope.SupplyItem.ModifiedDate = new Date;
        $scope.SupplyItem.ModifiedBy = $rootScope.AuthenticatedUser.Id;
        $scope.SupplyItem.IsNew = true;
    }

    function GetDropDowns() {
        //------ ddls ------\\
        itemsService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.ItemsList = response.data.ResponseObject;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

        itemsizeService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.ItemSizeList = response.data.ResponseObject;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

        raisingService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.RaisingList = response.data.ResponseObject;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

        schemeService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.SchemeList = response.data.ResponseObject;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
        //------ ddls ------\\
    }

    function SupplyFormValidate() {
        if (parseInt($scope.Supply.RaisingID) < 1 || parseInt($scope.Supply.SchemeID) < 1 || parseInt($scope.Supply.CustomerID) < 1 || $scope.Supply.SupplyDate.getDate() < 1 || $scope.Supply.SupplyDate === undefined || $scope.Supply.SupplyCoordinates.length < 1) {
            $scope.IsSupplyFormValid = false;
            return false;
        }

        if (parseInt($scope.Supply.CustomerID) === 1 || parseInt($scope.Supply.CustomerID) === 2) {
            if ($scope.Supply.Contact === '' || $scope.Supply.Contact === undefined) {
                $scope.IsSupplyFormValid = false;
                return false;
            }
        }

        if (parseInt($scope.Supply.CustomerID) === 1) {
            if (parseInt($scope.Supply.ArmedForcesID) < 1) {
                $scope.IsSupplyFormValid = false;
                return false;
            }
        }

        if (parseInt($scope.Supply.CustomerID) === 2) {
            if ($scope.Supply.CNIC === '' || $scope.Supply.CNIC === undefined || $scope.Supply.CustomerName === '' || $scope.Supply.CustomerName === undefined) {
                $scope.IsSupplyFormValid = false;
                return false;
            }
        }

        $scope.IsSupplyFormValid = true;
        return true;
    }

    function SupplItemFormValidate() {
        if (parseInt($scope.SupplyItem.ItemID) < 1 || parseInt($scope.SupplyItem.ItemSizeID) < 1 || $scope.SupplyItem.TotalQty < 1) {
            if ($scope.SupplyItem.TotalQty < 1) {
                $scope.TotalQtyError = "Total quantity must be greater than 0";
            } else {
                $scope.TotalQtyError = "";
            }
            $scope.IsSupplyItemFormValid = false;
            return false;
        }

        $scope.IsSupplyItemFormValid = true;
        return true;
    }

    function BindSupplyModel(supplyModel) {
        $scope.Supply.Id = supplyModel.Id;
        $scope.Supply.EmpID = supplyModel.EmpID;
        $scope.Supply.RaisingID = String(supplyModel.RaisingID);
        $scope.Supply.SchemeID = String(supplyModel.SchemeID);
        $scope.Supply.CustomerID = String(supplyModel.CustomerID);
        $scope.Supply.CustomerName = supplyModel.CustomerName;
        $scope.Supply.ArmedForcesID = String(supplyModel.ArmedForcesID);
        $scope.Supply.Contact = supplyModel.Contact;
        $scope.Supply.CNIC = supplyModel.CNIC;
        $scope.Supply.SupplyDate = supplyModel.SupplyDate;
        $scope.Supply.X = supplyModel.X;
        $scope.Supply.Y = supplyModel.Y;
        $scope.Supply.Active = supplyModel.Active;
        $scope.Supply.CreatedDate = supplyModel.CreatedDate;
        $scope.Supply.CreatedBy = supplyModel.CreatedBy;
        $scope.Supply.ModifiedDate = supplyModel.ModifiedDate;
        $scope.Supply.ModifiedBy = supplyModel.ModifiedBy;

        ItemCollectionFromServer = [];
        $scope.Supply.supplyItemCollection = [];
        $scope.Supply.SupplyCoordinates = supplyModel.SupplyCoordinates;


        if (supplyModel.supplyItemCollection !== null) {
            if (supplyModel.supplyItemCollection.length > 0) {
                for (var i = 0; i < supplyModel.supplyItemCollection.length; i++) {
                    SupplyItem = {
                        Id: supplyModel.supplyItemCollection[i].Id,
                        SupplyID: supplyModel.supplyItemCollection[i].SupplyID,
                        ItemID: supplyModel.supplyItemCollection[i].ItemID,
                        ItemSizeID: supplyModel.supplyItemCollection[i].ItemSizeID,
                        TotalQty: supplyModel.supplyItemCollection[i].TotalQty,
                        MicroQty: supplyModel.supplyItemCollection[i].MicroQty,
                        MacroQty: supplyModel.supplyItemCollection[i].MacroQty,
                        PottedQty: supplyModel.supplyItemCollection[i].PottedQty,
                        Active: supplyModel.supplyItemCollection[i].Active,
                        CreatedDate: supplyModel.supplyItemCollection[i].CreatedDate,
                        CreatedBy: supplyModel.supplyItemCollection[i].CreatedBy,
                        ModifiedDate: new Date,
                        ModifiedBy: $rootScope.AuthenticatedUser.Id,
                        IsNew: false
                    };

                    $scope.Supply.supplyItemCollection.push(SupplyItem);
                    ItemCollectionFromServer.push(angular.copy(SupplyItem));
                }
            }
        }
    }

    function MapModalInstanceCtrl($uibModalInstance) {
        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.GetLatLong = function (data) {
            $scope.Supply.SupplyCoordinates = [];
            for (var i = 0; i < data.length; i++) {
                $scope.Supply.SupplyCoordinates.push({ Id: 0, SupplyID: 0, PlantationName: data[i][2], Latitude: data[i][0], Longitude: data[i][1] });
            }
            $uibModalInstance.dismiss('cancel');
        }
    }

    function RenderSupplyReport() {
        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
            return supplyService.GetSupplyReport($rootScope.AuthenticatedUser.Id).then(function (response) {
                return response.data.ResponseObject;
            }, function (err) {
                return null;
            });
        })
            .withDOM("<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp")
            .withOption('createdRow', function (row, data, dataIndex) {
                $compile(angular.element(row))($scope);
                row.compiled = true;
            })
            .withButtons([
                { extend: 'copy', className: 'btn-sm' },
                { extend: 'csv', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'pdf', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ]);
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('EmpName').withTitle('Employee'),
            DTColumnBuilder.newColumn('ItemName').withTitle('Plant'),
            DTColumnBuilder.newColumn('ItemSize').withTitle('PlantSize'),
            DTColumnBuilder.newColumn('RaisingName').withTitle('Raising'),
            DTColumnBuilder.newColumn('CustomerName').withTitle('Customer'),
            DTColumnBuilder.newColumn('SchemeName').withTitle('Scheme'),
            DTColumnBuilder.newColumn('TotalQty').withTitle('No. Of Plants')
        ];

    }
}