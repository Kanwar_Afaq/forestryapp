﻿angular
    .module('homer')
    .controller('itemsCtrl', schemeCtrl);

function schemeCtrl($scope, $rootScope, $uibModal, notify, sweetAlert, itemsService, DTOptionsBuilder, DTColumnBuilder, $compile) {

    var Items = {
        Id: 0,
        Name: '',
        Descriptio: '',
        Active: false,
        CreatedBy: $rootScope.AuthenticatedUser.Id,
        CreatedDate: new Date,
        ModifiedBy: 0,
        ModifiedDate: new Date
    }
    var ItemModalInstance = null;

    RenderItems();

    $scope.itemDTInstance = {};

    $scope.Items = Items;

    $scope.DeleteItem = function (id) {
        sweetAlert.swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    itemsService.Delete(id).then(function (response) {
                        if (response.data.IsSuccess) {
                            notify({ message: response.data.Message, classes: 'alert-success' });
                            $scope.itemDTInstance.reloadData();
                        } else {
                            notify({ message: response.data.Message, classes: 'alert-danger' });
                        }
                    }, function (error) {
                        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
                    });
                }
            });
       
    }

    $scope.IsItemModelValid = true;

    $scope.AddNewItem = function () {
        ClearItemModel();
        ItemModalInstance = $uibModal.open({
            templateUrl: '/AppViews/modals/itemModal.html',
            controller: ItemModalInstanceCtrl,
            scope: $scope,
            size: 'md',
            windowClass: "hmodal-info"
        });
    }

    $scope.GetItemInfo = function (id) {
        itemsService.GetByID(id).then(function (response) {
            if (response.data.IsSuccess) {
                BindItemModel(response.data.ResponseObject);
                ItemModalInstance = $uibModal.open({
                    templateUrl: '/AppViews/modals/itemModal.html',
                    controller: ItemModalInstanceCtrl,
                    scope: $scope,
                    size: 'md',
                    windowClass: "hmodal-info"
                });
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    $scope.InsertUpdateItem = function () {
        if (ItemModelValidate()) {
            itemsService.InsertUpdate($scope.Items).then(function (response) {
                if (response.data.IsSuccess) {
                    $scope.itemDTInstance.reloadData();
                    ClearItemModel();
                    $scope.IsItemModelValid = true;
                    ItemModalInstance.dismiss('cancel');
                    notify({ message: response.data.Message, classes: 'alert-success' });
                } else {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                }
            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
        } else {
            $scope.IsItemModelValid = false;
        }
    }

    function RenderItems() {
        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
            return itemsService.GetAll().then(function (response) {
                if (response.data.ResponseObject !== null) {
                    return response.data.ResponseObject;
                }
            }, function (err) {
                return null;
            });
        })
            .withDOM("<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp")
            .withOption('createdRow', function (row, data, dataIndex) {
                $compile(angular.element(row))($scope);
                row.compiled = true;
            })
            .withButtons([
                { extend: 'copy', className: 'btn-sm' },
                { extend: 'csv', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'pdf', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ]);
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('Name').withTitle('Scheme Name'),
            DTColumnBuilder.newColumn('Description').withTitle('Description'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Created On'),
            DTColumnBuilder.newColumn('Id', 'Action')
                .renderWith(function (id) {
                    return "<a ng-click='GetItemInfo(" + id + ")'> <i class='fa fa-edit' title='Edit'></i></a> | <a ng-click='DeleteItem(" + id + ")'> <i class='fa fa-trash' title='Delete'></i></a>";
                }).notSortable()
        ];
    }

    function BindItemModel(itemModel) {
        $scope.Items.Id = itemModel.Id;
        $scope.Items.Name = itemModel.Name;
        $scope.Items.Description = itemModel.Description;
        $scope.Items.Active = itemModel.Active;
        $scope.Items.CreatedBy = itemModel.CreatedBy;
        $scope.Items.CreatedDate = itemModel.CreatedDate;
        $scope.Items.ModifiedBy = itemModel.ModifiedBy;
        $scope.Items.ModifiedDate = itemModel.ModifiedDate;
    }

    function ClearItemModel() {
        $scope.Items.Id = 0;
        $scope.Items.Name = '';
        $scope.Items.Description = '';
        $scope.Items.Active = '',
        $scope.Items.CreatedBy = $rootScope.AuthenticatedUser.Id;
        $scope.Items.CreatedDate = new Date;
        $scope.Items.ModifiedBy = 0;
        $scope.Items.ModifiedDate = new Date;
    }

    function ItemModelValidate() {
        if ($scope.Items.Name === '' || $scope.Items.Name === undefined) {
            return false;
        }

        return true;
    }

    function ItemModalInstanceCtrl($scope, $uibModalInstance) {

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };
}