﻿angular
    .module('homer')
    .controller('schemeCtrl', schemeCtrl);

function schemeCtrl($scope, $rootScope, $uibModal, notify, sweetAlert, schemeService, DTOptionsBuilder, DTColumnBuilder, $compile) {

    var Scheme = {
        Id: 0,
        Name: '',
        Active: false,
        CreatedBy: $rootScope.AuthenticatedUser.Id,
        CreatedDate: new Date,
        ModifiedBy: 0,
        ModifiedDate: new Date
    }
    var SchemeModalInstance = null;

    RenderSchemes();

    $scope.schemeDTInstance = {};

    $scope.Scheme = Scheme;

    $scope.DeleteScheme = function (id) {

        sweetAlert.swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    schemeService.Delete(id).then(function (response) {
                        if (response.data.IsSuccess) {
                            notify({ message: response.data.Message, classes: 'alert-success' });
                            $scope.schemeDTInstance.reloadData();
                        } else {
                            notify({ message: response.data.Message, classes: 'alert-danger' });
                        }
                    }, function (error) {
                        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
                    });
                }
            });
       
    }

    $scope.IsSchemeModelValid = true;

    $scope.AddNewScheme = function () {
        ClearSchemeModel();
        SchemeModalInstance = $uibModal.open({
            templateUrl: '/AppViews/modals/schemeModal.html',
            controller: SchemeModalInstanceCtrl,
            scope: $scope,
            size: 'md',
            windowClass: "hmodal-info"
        });
    }

    $scope.GetSchemeInfo = function (id) {
        schemeService.GetByID(id).then(function (response) {
            if (response.data.IsSuccess) {
                BindSchemeModel(response.data.ResponseObject);
                SchemeModalInstance = $uibModal.open({
                    templateUrl: '/AppViews/modals/schemeModal.html',
                    controller: SchemeModalInstanceCtrl,
                    scope: $scope,
                    size: 'md',
                    windowClass: "hmodal-info"
                });
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    $scope.InsertUpdateScheme = function () {
        if (SchemeModelValidate()) {
            schemeService.InsertUpdate($scope.Scheme).then(function (response) {
                if (response.data.IsSuccess) {
                    $scope.schemeDTInstance.reloadData();
                    ClearSchemeModel();
                    $scope.IsSchemeModelValid = true;
                    SchemeModalInstance.dismiss('cancel');
                    notify({ message: response.data.Message, classes: 'alert-success' });
                } else {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                }
            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
        } else {
            $scope.IsSchemeModelValid = false;
        }
    }

    function RenderSchemes() {
        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
            return schemeService.GetAll().then(function (response) {
                if (response.data.ResponseObject !== null) {
                    return response.data.ResponseObject;
                }
            }, function (err) {
                return null;
            });
        })
            .withDOM("<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp")
            .withOption('createdRow', function (row, data, dataIndex) {
                $compile(angular.element(row))($scope);
                row.compiled = true;
            })
            .withButtons([
                { extend: 'copy', className: 'btn-sm' },
                { extend: 'csv', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'pdf', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ]);
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('Name').withTitle('Scheme Name'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Created On'),
            DTColumnBuilder.newColumn('Id', 'Action')
                .renderWith(function (id) {
                    return "<a ng-click='GetSchemeInfo(" + id + ")'> <i class='fa fa-edit' title='Edit'></i></a> | <a ng-click='DeleteScheme(" + id + ")'> <i class='fa fa-trash' title='Delete'></i></a>";
                }).notSortable()
        ];
    }

    function BindSchemeModel(schemeModel) {
        $scope.Scheme.Id = schemeModel.Id;
        $scope.Scheme.Name = schemeModel.Name;
        $scope.Scheme.Active = schemeModel.Active;
        $scope.Scheme.CreatedBy = schemeModel.CreatedBy;
        $scope.Scheme.CreatedDate = schemeModel.CreatedDate;
        $scope.Scheme.ModifiedBy = schemeModel.ModifiedBy;
        $scope.Scheme.ModifiedDate = schemeModel.ModifiedDate;
    }

    function ClearSchemeModel() {
        $scope.Scheme.Id = 0;
        $scope.Scheme.Name = '';
        $scope.Scheme.Active = '',
        $scope.Scheme.CreatedBy = $rootScope.AuthenticatedUser.Id;
        $scope.Scheme.CreatedDate = new Date;
        $scope.Scheme.ModifiedBy = 0;
        $scope.Scheme.ModifiedDate = new Date;
    }

    function SchemeModelValidate() {
        if ($scope.Scheme.Name === '' || $scope.Scheme.Name === undefined) {
            return false;
        }

        return true;
    }

    function SchemeModalInstanceCtrl($scope, $uibModalInstance) {

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };
}