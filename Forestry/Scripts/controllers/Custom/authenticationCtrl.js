﻿angular
    .module('homer')
    .controller('authenticationCtrl', authenticationCtrl);

function authenticationCtrl($scope, $state, $rootScope, $cookies, notify, authenticationService) {

    if (authenticationService.IsEmployeeLoggedIn()) {
        $state.go('dashboard');
    }

    EmployeeModel = {
        FirstName: '',
        LastName: '',
        UserName: '',
        Password: '',
        Email: '',
        Address: '',
        DepartmentID: '',
        DesignationID: '',
        LocationID: '',
        RoleID: ''
    };

    $scope.EmployeeModel = EmployeeModel;
    $scope.IsRegistrationFormValid = true;
    $scope.IsForgotPassword = false;

    $scope.SendPasswordResetEmail = function () {
        $scope.resetPasswordLoading = true;

        authenticationService.SendResetPasswordEmail($scope.EmployeeModel.UserName).then(function (response) {
            if (response.data.IsSuccess) {
                notify({ message: response.data.Message, classes: 'alert-success' });
                $scope.resetPasswordLoading = false;
                $scope.IsForgotPassword = false;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
                $scope.resetPasswordLoading = false;
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            $scope.resetPasswordLoading = false;
        });
    }

    $scope.ForgotPassword = function () {
        $scope.IsForgotPassword = !$scope.IsForgotPassword;

        $scope.EmployeeModel.UserName = "";
        $scope.EmployeeModel.Password = "";

        //var original = $scope.EmployeeModel;
        //$scope.EmployeeModel = angular.copy(original);
        //$scope.LoginForm.$setUntouched();
        //$scope.ForgotPasswordForm.$setUntouched();
    }

    $scope.Login = function () {
        if (EmployeeModel.UserName === '' || EmployeeModel.Password === '' || EmployeeModel.UserName === undefined || EmployeeModel.Password === undefined) {
            $scope.IsRegistrationFormValid = false;
            return;
        }

        $scope.loginLoading = true;

        var response = authenticationService.Login(EmployeeModel).then(function (response) {
            if (response.data.IsSuccess) {
                $cookies.putObject('AuthenticatedUser', response.data.ResponseObject);
                $rootScope.AuthenticatedUser = $cookies.getObject('AuthenticatedUser');

                notify({ message: response.data.Message, classes: 'alert-success' });
                $scope.loginLoading = false;
                $state.go('dashboard');
            }
            else if (response.data.IsError) {
                $scope.loginLoading = false;
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
            else {
                $scope.loginLoading = false;
                $scope.LoginErr = response.data.Message;
            }
        }, function (err) {
            $scope.loginLoading = false;
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    };

    $scope.Register = function () {
        $scope.IsRegistrationFormValid = RegistrationFormValidate();

        if ($scope.IsRegistrationFormValid) {
            authenticationService.DoesUserNameExists(EmployeeModel.UserName).then(function (response) {
                if (response.data.IsSuccess) {
                    authenticationService.InsertEmployee(EmployeeModel).then(function (response) {
                        if (response.data.IsSuccess) {
                            notify({ message: response.data.Message, classes: 'alert-success' });
                            $state.go('login');
                        } else {
                            notify({ message: response.data.Message, classes: 'alert-danger' });
                        }
                    }, function (err) {
                        notify({ message: "Something went wrong on sending request.", classes: 'alert-danger' });
                    });
                }
                else if (response.data.IsError) {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                }
                else {
                    $scope.UserNameErr = response.data.Message;
                }
            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
        }
    };

    function RegistrationFormValidate() {
        if (EmployeeModel.UserName === '' || EmployeeModel.FirstName === '' || EmployeeModel.Password === '' || EmployeeModel.ConfirmPassword === '' || EmployeeModel.UserName === undefined || EmployeeModel.FirstName === undefined || EmployeeModel.Password === undefined || EmployeeModel.ConfirmPassword === undefined) {
            return false;
        }

        if (EmployeeModel.Password !== EmployeeModel.ConfirmPassword) {
            $scope.ConfirmPasswordErr = "Cofirm Password doesn't match with password"
            return false;
        } else {
            $scope.ConfirmPasswordErr = "";
        }

        return true;
    }
}

