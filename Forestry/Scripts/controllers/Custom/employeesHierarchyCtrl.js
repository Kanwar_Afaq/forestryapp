﻿angular
    .module('homer')
    .controller('employeesHierarchyCtrl', employeesHierarchyCtrl);

function employeesHierarchyCtrl($scope, $rootScope, employeesService, employeesHierarchyService, notify, sweetAlert) {
    EmployeeHierarchy = {
        Id: 0,
        ParentEmpID: "0",
        ChildEmpID: "0",
        CreatedDate: new Date,
        CreatedBy: $rootScope.AuthenticatedUser.Id,
        ModifiedDate: new Date,
        ModifiedBy: $rootScope.AuthenticatedUser.Id,
        Active: true,
        ChildEmployeesList: []
    };
    $scope.EmployeeHierarchy = EmployeeHierarchy;

    RenderEmployeeHierarchy();
    
    $scope.InsertUpdateEmployeeHierarchy = function () {
        employeesHierarchyService.InsertUpdate($scope.EmployeeHierarchy).then(function (response) {
            if (response.data.IsSuccess) {
                notify({ message: response.data.Message, classes: 'alert-success' });
                RenderEmployeeHierarchy();
                ClearEmployeeHierarchy();
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    $scope.GetChildEmployees = function () {
        employeesService.GetChildEmployees($scope.EmployeeHierarchy.ParentEmpID).then(function (response) {
            if (response.data.IsSuccess) {
                $scope.ChildEmployeesList = response.data.ResponseObject;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    };

    $scope.DeleteEmpHierarchy = function (id) {
        sweetAlert.swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    employeesHierarchyService.Delete(id).then(function (response) {
                        if (response.data.IsSuccess) {
                            notify({ message: response.data.Message, classes: 'alert-success' });
                            RenderEmployeeHierarchy();
                        } else {
                            notify({ message: response.data.Message, classes: 'alert-danger' });
                        }
                    }, function (error) {
                        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
                    });
                }
            });
        
    }

    ////// Get Dropdowns \\\\\\
    employeesService.GetCfAndCcf().then(function (response) {
        if (response.data.IsSuccess) {
            $scope.ParentEmployeesList = response.data.ResponseObject;
            RenderEmployeeHierarchy();
        } else {
            notify({ message: response.data.Message, classes: 'alert-danger' });
        }
    }, function (error) {
        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
    });
    ////// Get Dropdowns \\\\\\

    function RenderEmployeeHierarchy() {
        employeesHierarchyService.GetAll().then(function (response) {
            if (response.data.IsSuccess) {
                $scope.EmployeeHierarchyList = response.data.ResponseObject;
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    function ClearEmployeeHierarchy() {
        $scope.EmployeeHierarchy.Id= 0;
        $scope.EmployeeHierarchy.ParentEmpID= "0";
        $scope.EmployeeHierarchy.ChildEmpID= "0";
        $scope.EmployeeHierarchy.CreatedDate= new Date;
        $scope.EmployeeHierarchy.CreatedBy= $rootScope.AuthenticatedUser.Id;
        $scope.EmployeeHierarchy.ModifiedDate= new Date;
        $scope.EmployeeHierarchy.ModifiedBy= $rootScope.AuthenticatedUser.Id;
        $scope.EmployeeHierarchy.Active= true;
        $scope.EmployeeHierarchy.ChildEmployeesList= [];
    }
}
