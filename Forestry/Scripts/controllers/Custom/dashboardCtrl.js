﻿angular
    .module('homer')
    .controller('dashboardCtrl', dashboardCtrl);

function dashboardCtrl($scope, $rootScope, notify, dashboardService) {

    $scope.PlantableQty = 0;
    $scope.UnderSizeQty = 0;
    $scope.SuppliedQty = 0;

    $scope.MyEmployeesList = [];

    RenderDashboard();

    function RenderDashboard() {
        if ($rootScope.AuthenticatedUser !== null && $rootScope.AuthenticatedUser !== undefined) {
            dashboardService.GetAll($rootScope.AuthenticatedUser.Id).then(function (response) {
                if (response.data.IsSuccess) {
                    $scope.PlantableQty = response.data.ResponseObject.TotalPlantable;
                    $scope.UnderSizeQty = response.data.ResponseObject.TotalUnderSize;
                    $scope.SuppliedQty = response.data.ResponseObject.TotalSupplied;

                    $scope.MyEmployeesList = response.data.ResponseObject.EmployeesList;

                    RenderGraph(response.data.ResponseObject.MonthlySupplyList);
                } else {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                }
            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
        }


    }

    function RenderGraph(graphData) {
        $scope.lineProjectOptions = {
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            bezierCurve: false,
            pointDot: true,
            pointDotRadius: 3,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: false,
            datasetStrokeWidth: 1,
            datasetFill: true,
            responsive: true,
            tooltipTemplate: "<%= value %>",
            showTooltips: true,
            onAnimationComplete: function () {
                this.showTooltip(this.datasets[0].points, false);
            },
            tooltipEvents: []
        };

        $scope.lineProjectData = {
            labels: ["January", "February", "March", "April"],
            datasets: [
                {
                    label: "Example dataset",
                    fillColor: "rgba(98,203,49,0.5)",
                    strokeColor: "rgba(98,203,49,0.7)",
                    pointColor: "rgba(98,203,49,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [17, 21, 19, 24]
                }
            ]
        };
    }
}

