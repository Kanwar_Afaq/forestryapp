﻿angular
    .module('homer')
    .controller('mapCtrl', mapCtrl);

function mapCtrl($scope, $state, $rootScope, notify, supplyService) {

    if ($state.current.name === 'map') {

        RenderMapCoordinates();

    } else {
        $scope.DrawType = "amenity";

        var mymap = L.map('mapid').setView([30.3753, 69.3451], 5);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=sk.eyJ1IjoiYWxpaGFpZGVyOTUiLCJhIjoiY2pzMzQ1OHU3MGNkdDQzb2U4Ym1lZ3V2NSJ9.hp90NEuTxwKHpNi30cBKkg', {
            maxZoom: 20,
            id: 'mapbox.streets',
            accessToken: 'sk.eyJ1IjoiYWxpaGFpZGVyOTUiLCJhIjoiY2pzMzQ1OHU3MGNkdDQzb2U4Ym1lZ3V2NSJ9.hp90NEuTxwKHpNi30cBKkg'
        }).addTo(mymap);

        var CoordinateList = [];
        var isNewPolygon = false;
        var prevDdl = 'amenity';
        var circle = null;
        var straightline = null;
        var polygon = null;

        $scope.ChangeDrawType = function () {
            CoordinateList = [];
            if (prevDdl === 'canalside') {
                if (circle !== null) {
                    mymap.removeLayer(circle);
                }
                prevDdl = $scope.DrawType;
            }

            if (prevDdl === 'roadside') {
                if (straightline !== null) {
                    mymap.removeLayer(straightline);
                }
                prevDdl = $scope.DrawType;
            }

            if (prevDdl === 'amenity') {
                if (polygon !== null) {
                    mymap.removeLayer(polygon);
                }
                prevDdl = $scope.DrawType;
            }
        }

        $scope.SavePolygon = function () {
            isNewPolygon = true;
            if (polygon !== null) {
                mymap.removeLayer(polygon);
            }
            polygon = L.polygon([
                CoordinateList
            ], { color: 'blue' }).addTo(mymap);

            $scope.Coordinates = CoordinateList;
        }


        mymap.on('click', function (e) {
            if ($scope.DrawType === "amenity") {
                if (isNewPolygon) {
                    CoordinateList = [];
                }
                isNewPolygon = false;
                CoordinateList.push([e.latlng.lat, e.latlng.lng, $scope.DrawType]);
            } else if ($scope.DrawType === "roadside") {
                CoordinateList.push([e.latlng.lat, e.latlng.lng, $scope.DrawType]);

                if (CoordinateList.length > 2) {
                    mymap.removeLayer(straightline);
                    CoordinateList.shift();
                    CoordinateList.shift();
                }


                if (CoordinateList.length > 1) {
                    straightline = L.polygon([
                        CoordinateList[0],
                        CoordinateList[1]
                    ], { color: 'red' }).addTo(mymap);


                    $scope.Coordinates = CoordinateList;
                }

            } else if ($scope.DrawType === "canalside") {
                CoordinateList = [e.latlng.lat, e.latlng.lng, $scope.DrawType];
                if (circle !== null) {
                    mymap.removeLayer(circle);
                }
                circle = L.circle(CoordinateList, {
                    color: 'green',
                    fillColor: 'green',
                    fillOpacity: 0.5,
                    radius: 10000
                }).addTo(mymap);


                $scope.Coordinates = Array(CoordinateList);

            }
        });



        setTimeout(function () {
            mymap.invalidateSize();
        }, 10);
    }

    function RenderMapCoordinates() {
        supplyService.GetMapCoordinates($rootScope.AuthenticatedUser.Id).then(function (response) {
            if (response.data.IsSuccess) {
                DrawCoordinates(response.data.ResponseObject);
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    function DrawCoordinates(data) {

        var mymap = L.map('mapid').setView([30.3753, 69.3451], 5);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=sk.eyJ1IjoiYWxpaGFpZGVyOTUiLCJhIjoiY2pzMzQ1OHU3MGNkdDQzb2U4Ym1lZ3V2NSJ9.hp90NEuTxwKHpNi30cBKkg', {
            maxZoom: 20,
            id: 'mapbox.streets',
            accessToken: 'sk.eyJ1IjoiYWxpaGFpZGVyOTUiLCJhIjoiY2pzMzQ1OHU3MGNkdDQzb2U4Ym1lZ3V2NSJ9.hp90NEuTxwKHpNi30cBKkg'
        }).addTo(mymap);

        if (data !== null) {
            for (var i = 0; i < data.length; i++) {
        if (data[i].PlantationName === 'amenity') {
            if (data[i].LatLngArr.length > 0) {
                L.polygon(data[i].LatLngArr, { color: 'blue' }).bindPopup("Plantation: Amenity <br/> By: " +data[i].EmpName).addTo(mymap);
            }
        } else if (data[i].PlantationName === 'roadside') {
            if (data[i].LatLngArr.length > 0) {
                L.polygon(data[i].LatLngArr, { color: 'red' }).bindPopup("Plantation: Roadsize <br/> By: " + data[i].EmpName).addTo(mymap);
            }
        } else if (data[i].PlantationName === 'canalside') {
            if (data[i].LatLngArr.length > 0) {

                L.circle(data[i].LatLngArr[0], {
                    color: 'green',
                    fillColor: 'green',
                    fillOpacity: 0.5,
                    radius: 10000
                }).bindPopup("Plantation: Canalside <br/> By: " + data[i].EmpName).addTo(mymap);
            }
                }

            }
        }
    }
}