﻿angular
    .module('homer')
    .controller('nurseryinventoryCtrl', nurseryinventoryCtrl);

function nurseryinventoryCtrl($scope, $rootScope, $state, $compile, $uibModal, notify, sweetAlert, nurseryinventoryService, DTOptionsBuilder, DTColumnBuilder, itemsService, itemsizeService, raisingService, schemeService) {

    var ItemInventory = {
        Id: 0,
        ItemID: "0",
        EmpID: $rootScope.AuthenticatedUser.Id,
        ItemSizeID: "0",
        TotalQty: 0,
        MicroQty: 0,
        MacroQty: 0,
        PottedQty: 0,
        RaisingID: "0",
        SchemeID: "0",
        Active: false,
        CreatedDate: new Date,
        CreatedBy: $rootScope.AuthenticatedUser.Id,
        ModifiedDate: new Date,
        ModifiedBy: 0
    }
    var ItemInventoryModalInstance = null;

    $scope.ItemInventoryDTInstance = {};

    $scope.SchemeList = [];
    $scope.ItemSizeList = [];
    $scope.ItemsList = [];
    $scope.RaisingList = [];

    if ($state.current.name === 'inventoryreport') {
        RenderInventoryReport();
    } else {
        RenderItemInventory();
    }

    $scope.GetTotal = function () {
        $scope.ItemInventory.TotalQty = $scope.ItemInventory.MicroQty + $scope.ItemInventory.MacroQty + $scope.ItemInventory.PottedQty;
    }
    
    $scope.IsItemInventoryValid = true;
    
    $scope.ItemInventory = ItemInventory;

    $scope.InsertUpdateItemInventory = function () {
        if (ItemInventoryValidate()) {
            nurseryinventoryService.InsertUpdate($scope.ItemInventory).then(function (response) {
                if (response.data.IsSuccess) {
                    notify({ message: response.data.Message, classes: 'alert-success' });
                    ClearItemInventoryModel();
                    ItemInventoryModalInstance.dismiss('cancel');
                    $scope.ItemInventoryDTInstance.reloadData();
                } else {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                }
            }, function (response) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
        } else {
            $scope.IsItemInventoryValid = false;
        }
    }

    $scope.GetInventoryItemInfo = function (id) {
        nurseryinventoryService.GetByID(id).then(function (response) {
            if (response.data.IsSuccess) {
                BindItemInveryModel(response.data.ResponseObject);
                ItemInventoryModalInstance = $uibModal.open({
                    templateUrl: '/AppViews/modals/itemInventoryModal.html',
                    controller: ItemInventoryModalInstanceCtrl,
                    scope: $scope,
                    size: 'lg',
                    windowClass: "hmodal-info"
                });
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (response) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    $scope.AddNewItem = function(){
        ClearItemInventoryModel();
        ItemInventoryModalInstance = $uibModal.open({
            templateUrl: '/AppViews/modals/itemInventoryModal.html',
            controller: ItemInventoryModalInstanceCtrl,
            scope: $scope,
            size: 'lg',
            windowClass: "hmodal-info"
        });
    }

    $scope.DeleteItemInventory = function (id) {

         sweetAlert.swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true },
            function (isConfirm) {
                if (isConfirm) {
                    nurseryinventoryService.Delete(id).then(function (response) {
                        if (response.data.IsSuccess) {
                            notify({ message: response.data.Message, classes: 'alert-success' });
                            $scope.ItemInventoryDTInstance.reloadData();
                        } else {
                            notify({ message: response.data.Message, classes: 'alert-danger' });
                        }
                    }, function (response) {
                        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
                    });
                }
            });
     
    }

    //------ ddls ------\\
    itemsService.GetAll().then(function (response) {
        if (response.data.IsSuccess) {
            $scope.ItemsList = response.data.ResponseObject;
        } else {
            notify({ message: response.data.Message, classes: 'alert-danger' });
        }
    }, function (error) {
        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

    itemsizeService.GetAll().then(function (response) {
        if (response.data.IsSuccess) {
            $scope.ItemSizeList = response.data.ResponseObject;
        } else {
            notify({ message: response.data.Message, classes: 'alert-danger' });
        }
    }, function (error) {
        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

    raisingService.GetAll().then(function (response) {
        if (response.data.IsSuccess) {
            $scope.RaisingList = response.data.ResponseObject;
        } else {
            notify({ message: response.data.Message, classes: 'alert-danger' });
        }
    }, function (error) {
        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

    schemeService.GetAll().then(function (response) {
        if (response.data.IsSuccess) {
            $scope.SchemeList = response.data.ResponseObject;
        } else {
            notify({ message: response.data.Message, classes: 'alert-danger' });
        }
    }, function (error) {
        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
    });
    //------ ddls ------\\

    function BindItemInveryModel(itemInventoryModel) {
        $scope.ItemInventory.Id = itemInventoryModel.Id;
        $scope.ItemInventory.ItemID = String(itemInventoryModel.ItemID);
        $scope.ItemInventory.EmpID = itemInventoryModel.EmpID;
        $scope.ItemInventory.ItemSizeID = String(itemInventoryModel.ItemSizeID);
        $scope.ItemInventory.TotalQty = itemInventoryModel.TotalQty;
        $scope.ItemInventory.MicroQty = itemInventoryModel.MicroQty;
        $scope.ItemInventory.MacroQty = itemInventoryModel.MacroQty;
        $scope.ItemInventory.PottedQty = itemInventoryModel.PottedQty;
        $scope.ItemInventory.RaisingID = String(itemInventoryModel.RaisingID);
        $scope.ItemInventory.SchemeID = String(itemInventoryModel.SchemeID);
        $scope.ItemInventory.Active = itemInventoryModel.Active;
        $scope.ItemInventory.CreatedDate = itemInventoryModel.CreatedDate;
        $scope.ItemInventory.CreatedBy = itemInventoryModel.CreatedBy;
        $scope.ItemInventory.ModifiedDate = new Date;
        $scope.ItemInventory.ModifiedBy = $rootScope.AuthenticatedUser.Id;
    }

    function ClearItemInventoryModel() {
            $scope.ItemInventory.Id= 0;
            $scope.ItemInventory.ItemID= "0";
            $scope.ItemInventory.EmpID= $rootScope.AuthenticatedUser.Id;
            $scope.ItemInventory.ItemSizeID= "0";
            $scope.ItemInventory.TotalQty= 0;
            $scope.ItemInventory.MicroQty= 0;
            $scope.ItemInventory.MacroQty= 0;
            $scope.ItemInventory.PottedQty= 0;
            $scope.ItemInventory.RaisingID= "0";
            $scope.ItemInventory.SchemeID= "0";
            $scope.ItemInventory.Active= false;
            $scope.ItemInventory.CreatedDate= new Date;
            $scope.ItemInventory.CreatedBy= $rootScope.AuthenticatedUser.Id;
            $scope.ItemInventory.ModifiedDate= new Date;
            $scope.ItemInventory.ModifiedBy= 0;
    }

    function ItemInventoryValidate() {
        if ($scope.ItemInventory.ItemID === "0" || $scope.ItemInventory.ItemSizeID === "0" ||
            $scope.ItemInventory.RaisingID === "0" || $scope.ItemInventory.TotalQty < 1) {
            return false;
        }

        return true;
    }

    function RenderItemInventory() {
        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
            return nurseryinventoryService.GetByEmployee($rootScope.AuthenticatedUser.Id).then(function (response) {
                GetPlantableAndUnderSize(response.data.ResponseObject);
                return response.data.ResponseObject;
            }, function (err) {
                return null;
            });
        })
            .withDOM("<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp")
            .withOption('createdRow', function (row, data, dataIndex) {
                $compile(angular.element(row))($scope);
                row.compiled = true;
            })
            .withButtons([
                { extend: 'copy', className: 'btn-sm' },
                { extend: 'csv', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'pdf', title: 'ExampleFile', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ]);
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('ItemName').withTitle('Item'),
            DTColumnBuilder.newColumn('ItemSize').withTitle('Size'),
            DTColumnBuilder.newColumn('TotalQty').withTitle('Total Quantity'),
            DTColumnBuilder.newColumn('MicroQty').withTitle('Micro Quantity'),
            DTColumnBuilder.newColumn('MacroQty').withTitle('Macro Quantity'),
            DTColumnBuilder.newColumn('PottedQty').withTitle('Potted Quantity'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Created On'),
            DTColumnBuilder.newColumn('Id', 'Action')
                .renderWith(function (id) {
                    return "<a ng-click='GetInventoryItemInfo(" + id + ")'><i class='fa fa-edit' title='Edit'></i></a> | <a ng-click='DeleteItemInventory(" + id + ")'><i class='fa fa-trash' title='Delete'></i></a>";
                        }).notSortable()
        ];
    }

    function GetPlantableAndUnderSize(itemIventoryList) {
        $scope.TotalPlantable = 0;
        $scope.TotalUnderSize = 0;
        for (var i = 0; i < itemIventoryList.length; i++) {
            if (itemIventoryList[i].ItemSize === "Plantable") {
                $scope.TotalPlantable += itemIventoryList[i].TotalQty;
            } else {
                $scope.TotalUnderSize += itemIventoryList[i].TotalQty;
            }
        }
    }

    function ItemInventoryModalInstanceCtrl($scope, $uibModalInstance) {

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }


    function RenderInventoryReport() {
                $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    return nurseryinventoryService.GetInventoryReport($rootScope.AuthenticatedUser.Id).then(function (response) {
                        GetPlantableAndUnderSize(response.data.ResponseObject);
                        return response.data.ResponseObject;
                    }, function (err) {
                        return null;
                    });
                })
                    .withDOM("<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp")
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row))($scope);
                        row.compiled = true;
                    })
                    .withButtons([
                        { extend: 'copy', className: 'btn-sm' },
                        { extend: 'csv', title: 'ExampleFile', className: 'btn-sm' },
                        { extend: 'pdf', title: 'ExampleFile', className: 'btn-sm' },
                        { extend: 'print', className: 'btn-sm' }
                    ]);
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn('EmpName').withTitle('Employee'),
                    DTColumnBuilder.newColumn('ItemName').withTitle('Plant'),
                    DTColumnBuilder.newColumn('ItemSize').withTitle('Plant Size'),
                    DTColumnBuilder.newColumn('MicroQty').withTitle('Micro Quantity'),
                    DTColumnBuilder.newColumn('MacroQty').withTitle('Macro Quantity'),
                    DTColumnBuilder.newColumn('PottedQty').withTitle('Potted Quantity'),
                    DTColumnBuilder.newColumn('TotalQty').withTitle('Total Quantity')
                ];
    }
}
