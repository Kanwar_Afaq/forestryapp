﻿angular
    .module('homer')
    .controller('employeesCtrl', employeesCtrl)


function employeesCtrl($scope, $compile, $location, $stateParams, $uibModal, $state, $rootScope, DTOptionsBuilder, DTColumnBuilder, employeesService, authenticationService, notify, rolesService, locationService, designationService) {

    var EmployeeList = [];
    var EmployeeModel = {
        Id: 0,
        FirstName: '',
        LastName: '',
        Email: '',
        Address: '',
        Password: '',
        ConfirmPassword: '',
        RoleID: "0",
        LocationID: "0",
        DepartmentID: "0",
        DesignationID: "0",
        Active: false
    };
    var EmployeeAssignmentModalInstance = null;

    $scope.IsEmployeeFormValid = true;
    IsChangePasswordFormValid = true;

    RenderEmployees();

    $scope.employeesDTInstance = {};
    $scope.EmployeeModel = EmployeeModel;

    ////// Get DDls \\\\\\
    rolesService.GetAll().then(function (response) {
        if (response.data.IsSuccess) {
            $scope.RoleList = response.data.ResponseObject;
        } else {
            notify({ message: response.data.Message, classes: 'alert-danger' });
        }
    }, function (error) {
        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

    locationService.GetAll().then(function (response) {
        if (response.data.IsSuccess) {
            $scope.LocationList = response.data.ResponseObject;
        } else {
            notify({ message: response.data.Message, classes: 'alert-danger' });
        }
    }, function (error) {
        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });

    designationService.GetAll().then(function (response) {
        if (response.data.IsSuccess) {
            $scope.DesignationList = response.data.ResponseObject;
        } else {
            notify({ message: response.data.Message, classes: 'alert-danger' });
        }
    }, function (error) {
        notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });


    $scope.DepartmentList = [
        {
            ID: 1,
            Name: 'Department-1'
        },
        {
            ID: 2,
            Name: 'Department-2'
        },
        {
            ID: 3,
            Name: 'Department-3'
        }
    ]
    ////// Get DDls \\\\\\



    $scope.GetEmployeeInfo = function (id) {
        employeesService.GetEmployeeByID(id).then(function (response) {
            if (response.data.IsSuccess) {
                BindEmployeeData(response.data.ResponseObject);
                if ($state.current.name !== 'profile') {
                    EmployeeAssignmentModalInstance = $uibModal.open({
                        templateUrl: '/AppViews/modals/employeeAssignmentModal.html',
                        controller: ModalInstanceCtrl,
                        scope: $scope,
                        size: 'md',
                        windowClass: "hmodal-info"
                    });
                }
            } else if (response.data.IsError) {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (error) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    $scope.UpdateEmployee = function (isAssignments) {
        employeesService.UpdateEmployee($scope.EmployeeModel, isAssignments).then(function (response) {
            if (response.data.IsSuccess) {
                notify({ message: response.data.Message, classes: 'alert-success' });
                if(EmployeeAssignmentModalInstance !== null){
                    EmployeeAssignmentModalInstance.dismiss('cancel');
                }
                $scope.employeesDTInstance.reloadData();
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (err) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    $scope.ActiveDeactiveEmployee = function (id) {
        employeesService.ActivateDeactivateEmployee(id).then(function (response) {
            if (response.data.IsSuccess) {
                $scope.employeesDTInstance.reloadData();
            } else {
                notify({ message: response.data.Message, classes: 'alert-danger' });
            }
        }, function (err) {
            notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
        });
    }

    $scope.IsEmployeeActive = function (id) {
        return EmployeeList.filter(x => x.Id === id)[0].Active;
    }

    $scope.IsForgotPassword = false;

    if (!authenticationService.IsEmployeeLoggedIn()) {
        if ($stateParams.q !== '' && $stateParams.q !== undefined && $stateParams.eid !== '' && $stateParams.eid !== undefined) {
            $scope.IsForgotPassword = true;
            authenticationService.IsChangePasswordKeyValid($stateParams.q, $stateParams.eid).then(function (response) {
                if (!response.data.IsSuccess) {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                    $state.go('login');
                }
            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
                $state.go('login');
            });
        } else {
            if (!authenticationService.IsEmployeeLoggedIn()) {
                $state.go('login');
            }
        }
    }

    $scope.ChangePassword = function () {
        if (ChangePasswordFormValidate()) {
            authenticationService.ChangePassword($scope.IsForgotPassword ? $stateParams.eid : $rootScope.AuthenticatedUser.Id, $scope.EmployeeModel.Password, $scope.EmployeeModel.ConfirmPassword, $scope.CurrentPassword, $scope.IsForgotPassword).then(function (response) {
                if (response.data.IsSuccess) {
                    notify({ message: response.data.Message, classes: 'alert-success' });
                    authenticationService.Logout();
                    $location.path('login');
                } else {
                    notify({ message: response.data.Message, classes: 'alert-danger' });
                }
            }, function (error) {
                notify({ message: 'Something went wrong on sending request.', classes: 'alert-danger' });
            });
        }
    }

    if ($state.current.name === 'profile') {
        $scope.GetEmployeeInfo($rootScope.AuthenticatedUser.Id);
    }

    function RenderEmployees() {
        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
            return employeesService.GetAllEmployees().then(function (response) {
                EmployeeList = response.data.ResponseObject;
                return response.data.ResponseObject;
            }, function (err) {
                return null;
            });
        })
        .withDOM("<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp")
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row))($scope);
            row.compiled = true;
        })
        .withButtons([
            { extend: 'copy', className: 'btn-sm' },
            { extend: 'csv', title: 'ExampleFile', className: 'btn-sm' },
            { extend: 'pdf', title: 'ExampleFile', className: 'btn-sm' },
            { extend: 'print', className: 'btn-sm' }
        ]);
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('FirstName').withTitle('First Name'),
            DTColumnBuilder.newColumn('LastName').withTitle('Last Name'),
            DTColumnBuilder.newColumn('Email').withTitle('Email'),
              DTColumnBuilder.newColumn('Active', 'Active')
                .renderWith(function (active) {
                    return (active ? 'Yes' : 'No')
                }),
            DTColumnBuilder.newColumn('Id', 'Action')
                .renderWith(function (id) {
                    return "<a ng-click='GetEmployeeInfo(" + id + ")'><i class='fa fa-edit' title='Edit'></i></a> | <a ng-click='ActiveDeactiveEmployee(" + id + ")'> <i class='fa fa-user-times text-danger' ng-if='IsEmployeeActive(" + id + ")' title='Deativate'></i> <i class='fa fa-user text-success' ng-if='!IsEmployeeActive(" + id + ")' title='Activate'></i></a>"
                }).notSortable()
        ];
    }

    function ModalInstanceCtrl($scope, $uibModalInstance) {

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };

    function BindEmployeeData(employeeModel) {
        $scope.EmployeeModel.Id = employeeModel.Id;
        $scope.EmployeeModel.FirstName = employeeModel.FirstName;
        $scope.EmployeeModel.LastName = employeeModel.LastName;
        $scope.EmployeeModel.Email = employeeModel.Email;
        $scope.EmployeeModel.Address = employeeModel.Address;
        if ($state.current.name !== 'profile') {
            $scope.EmployeeModel.Password = employeeModel.Password;
        }
        $scope.EmployeeModel.ConfirmPassword = '';
        $scope.EmployeeModel.RoleID = employeeModel.RoleID !== null? String(employeeModel.RoleID) : "0";
        $scope.EmployeeModel.LocationID = employeeModel.LocationID !== null ? String(employeeModel.LocationID) : "0";
        $scope.EmployeeModel.DepartmentID = employeeModel.DepartmentID !== null ? String(employeeModel.DepartmentID) : "0";
        $scope.EmployeeModel.DesignationID = employeeModel.DesignationID !== null ? String(employeeModel.DesignationID) : "0";
        $scope.EmployeeModel.Active = employeeModel.Active;
    }

    function EmployeeFormValidate() {
        if ($state.current.name === 'profile') {
            if ($scope.EmployeeModel.FirstName === '' || $scope.EmployeeModel.FirstName === undefined) {
                return false;
            }
        }

        $scope.IsEmployeeFormValid = true;
        return true;
    }

    function ChangePasswordFormValidate() {
        if (!$scope.IsForgotPassword) {
            if ($scope.CurrentPassword === '' || $scope.CurrentPassword === undefined) {
                $scope.IsChangePasswordFormValid = false;
                return false;
            }
        }

        if ($scope.EmployeeModel.Password === '' ||  $scope.EmployeeModel.ConfirmPassword !== $scope.EmployeeModel.Password || $scope.EmployeeModel.Password === undefined) {
            $scope.IsChangePasswordFormValid = false;
            return false;
        }
        else {
            $scope.IsChangePasswordFormValid = true;
            return true;
        }
    }
}