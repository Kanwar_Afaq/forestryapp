﻿angular
    .module('homer')
    .factory('itemsService', itemsService);

function itemsService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/Items/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        },

        InsertUpdate: function (itemModel) {
            return $http({
                url: "/Items/InsertUpdate",
                data: { 'itemModel': itemModel },
                dataType: 'json',
                method: 'POST'
            });
        },

        GetByID: function (id) {
            return $http({
                url: "/Items/GetByID",
                params: { 'id': id },
                dataType: 'json',
                method: 'GET'
            });
        },

        Delete: function (id) {
            return $http({
                url: "/Items/Delete",
                data: { 'id': id },
                dataType: 'json',
                method: 'POST'
            });
        }
    }
}