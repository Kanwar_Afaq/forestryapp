﻿angular
    .module('homer')
    .factory('employeesService', employeesService);

function employeesService($http) {
    return {
        GetAllEmployees: function () {
            return $http({
                url: "/Employees/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        },

        GetEmployeeByID: function (id) {
            return $http({
                url: "/Employees/GetEmployeeByID",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        },

        GetChildEmployees: function (id) {
            return $http({
                url: "/Employees/GetChildEmployees",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        },

        GetCfAndCcf: function () {
            return $http({
                url: "/Employees/GetCfAndCcf",
                dataType: 'json',
                method: 'GET'
            });
        },

        ActivateDeactivateEmployee: function (id) {
            return $http({
                url: "/Employees/ActivateDeactivateEmployee",
                dataType: 'json',
                params: { 'id': id },
                method: 'POST'
            });
        },

        UpdateEmployee: function (employeeModel, isAssignment) {
            return $http({
                url: "/Employees/UpdateEmployee",
                dataType: 'json',
                data: { 'employeeModel': employeeModel, 'isAssignment': isAssignment },
                method: 'POST'
            });
        }
    }
}