﻿angular
    .module('homer')
    .factory('supplyService', supplyService);

function supplyService($http) {
    return {

        InsertUpdate: function (supplyModel) {
            return $http({
                url: "/Supply/InsertUpdate",
                dataType: 'json',
                data: { 'supplyModel': supplyModel, 'supplyItems': supplyModel.supplyItemCollection, 'supplyCoordinates': supplyModel.SupplyCoordinates },
                method: 'POST'
            });
        },

        GetByID: function (id) {
            return $http({
                url: "/Supply/GetByID",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        },

        GetByEmployee: function (id) {
            return $http({
                url: "/Supply/GetByEmployee",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        },

        Delete: function (id) {
            return $http({
                url: "/Supply/Delete",
                dataType: 'json',
                params: { 'id': id },
                method: 'POST'
            });
        },

        DeleteSupplyItem: function (id) {
            return $http({
                url: "/Supply/DeleteSupplyItem",
                dataType: 'json',
                params: { 'id': id },
                method: 'POST'
            });
        },

        IsItemAvailable: function (empID, itemID, itemSizeID, microQty, macroQty, pottedQty) {
            return $http({
                url: "/Supply/IsItemAvailable",
                dataType: 'json',
                params: { 'empID': empID, 'itemID': itemID, 'itemSizeID': itemSizeID, 'microQty': microQty, 'macroQty': macroQty, 'pottedQty': pottedQty },
                method: 'GET'
            });
        },

        GetSupplyReport: function (id) {
            return $http({
                url: "/Supply/GetSupplyReport",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        },

        GetMapCoordinates: function (id) {
            return $http({
                url: "/Supply/GetMapCoordinates",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        }

    }
}