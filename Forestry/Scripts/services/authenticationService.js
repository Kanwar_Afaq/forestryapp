﻿angular
    .module('homer')
    .factory('authenticationService', authenticationService);

function authenticationService($rootScope, $cookies, $http) {

    return {
        IsEmployeeLoggedIn: function () {
            if ($rootScope.AuthenticatedUser !== null && $rootScope.AuthenticatedUser !== undefined) {
                return true;
            }
            else {
                return false;
            }
        },

        Login: function (EmployeeModel) {
            return $http({
                url: "/Account/Authenticate",
                data: { 'employeeModel': EmployeeModel },
                dataType: 'json',
                method: 'POST'
            });
        },

        Logout: function(){
            $cookies.remove('AuthenticatedUser');
            $rootScope.AuthenticatedUser = $cookies.getObject('AuthenticatedUser');
        },

        DoesUserNameExists: function (UserName) {
            return $http({
                url: "/Account/IsUsernameAvailable",
                params: { 'userName' : UserName },
                dataType: 'json',
                method: 'GET'
            });
        },

        InsertEmployee: function (EmployeeModel) {
            return $http({
                url: "/Account/Insert",
                data: { 'employeeModel': EmployeeModel },
                dataType: 'json',
                method: 'POST'
            });
        },

        SendResetPasswordEmail: function (UserName) {
            return $http({
                url: "/Account/SendResetPasswordEmail",
                params: { 'userName': UserName },
                dataType: 'json',
                method: 'POST'
            });
        },

        ChangePassword: function (empID, password, confirmPassword, oldPassword, isForgotPassword) {
            return $http({
                url: "/Account/ChangePassword",
                params: { 'empID': empID, 'password': password, 'confirmPassword': confirmPassword, 'oldPassword': oldPassword, 'isForgotPassword': isForgotPassword },
                dataType: 'json',
                method: 'POST'
            });
        },

        IsPageAllowed: function (pageName, designationID, roleID) {
            return $http({
                url: "/Account/IsPageAllowed",
                params: { 'pageName': pageName, 'designationID': designationID, 'roleID': roleID },
                dataType: 'json',
                method: 'GET'
            });
        },

        IsChangePasswordKeyValid: function (key, empID) {
            return $http({
                url: "/Account/IsChangePasswordKeyValid",
                params: { 'empID': empID, 'key': key },
                dataType: 'json',
                method: 'GET'
            });
        }
    }
}