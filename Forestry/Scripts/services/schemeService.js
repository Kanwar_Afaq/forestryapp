﻿angular
    .module('homer')
    .factory('schemeService', schemeService);

function schemeService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/Scheme/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        },

        InsertUpdate: function (schemeModel) {
            return $http({
                url: "/Scheme/InsertUpdate",
                data: { 'schemeModel': schemeModel },
                dataType: 'json',
                method: 'POST'
            });
        },

        GetByID: function (id) {
            return $http({
                url: "/Scheme/GetByID",
                params: { 'id': id },
                dataType: 'json',
                method: 'GET'
            });
        },

        Delete: function (id) {
            return $http({
                url: "/Scheme/Delete",
                data: { 'id': id },
                dataType: 'json',
                method: 'POST'
            });
        }
    }
}