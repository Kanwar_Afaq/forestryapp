﻿angular
    .module('homer')
    .factory('locationService', locationService);

function locationService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/Location/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        }
    }
}