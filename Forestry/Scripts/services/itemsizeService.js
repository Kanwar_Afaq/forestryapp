﻿angular
    .module('homer')
    .factory('itemsizeService', itemsizeService);

function itemsizeService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/ItemSize/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        }
    }
}