﻿angular
    .module('homer')
    .factory('raisingService', raisingService);

function raisingService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/Raising/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        }
    }
}