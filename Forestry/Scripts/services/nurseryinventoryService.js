﻿angular
    .module('homer')
    .factory('nurseryinventoryService', nurseryinventoryService);

function nurseryinventoryService($http){
    return {
        InsertUpdate: function (itemInventoryModel){
            return $http({
                url: "/ItemInventory/InsertUpdateItem",
                dataType: 'json',
                data: { 'itemInventoryModel': itemInventoryModel },
                method: 'POST'
            });
        },

        GetByEmployee: function (id) {
            return $http({
                url: "/ItemInventory/GetByEmployee",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        },

        GetByEmployeeSum: function (id) {
            return $http({
                url: "/ItemInventory/GetByEmployeeSum",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        },

        GetByID: function (id) {
            return $http({
                url: "/ItemInventory/GetByID",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        },

        Delete: function (id) {
            return $http({
                url: "/ItemInventory/Delete",
                dataType: 'json',
                params: { 'id': id },
                method: 'POST'
            });
        },

        GetInventoryReport: function (id) {
            return $http({
                url: "/ItemInventory/GetInventoryReport",
                dataType: 'json',
                params: { 'id': id },
                method: 'GET'
            });
        }
    }
}
