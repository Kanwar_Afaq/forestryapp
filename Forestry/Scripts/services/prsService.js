﻿angular
    .module('homer')
    .factory('prsService', prsService);

function prsService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/PRS/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        },

        GetBySupply: function (id) {
            return $http({
                url: "/PRS/GetBySupply",
                params: {'id': id},
                dataType: 'json',
                method: 'GET'
            });
        },

        InsertUpdate(prsModel) {
            return $http({
                url: "/PRS/InsertUpdate",
                data: { 'prsModel': prsModel, 'prsItemsCollection': prsModel.PRSItemCollection },
                dataType: 'json',
                method: 'POST'
            });
        }
    }
}