﻿angular
    .module('homer')
    .factory('employeesHierarchyService', employeesHierarchyService);

function employeesHierarchyService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/EmployeeHierarchy/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        },

        InsertUpdate: function (employeeHierarchyModel) {
            return $http({
                url: "/EmployeeHierarchy/InsertUpdate",
                dataType: 'json',
                data: { 'employeeHierarchyModel': employeeHierarchyModel },
                method: 'POST'
            });
        },

        Delete: function (id) {
            return $http({
                url: "/EmployeeHierarchy/Delete",
                dataType: 'json',
                params: { 'id': id },
                method: 'POST'
            });
        }
    }
}