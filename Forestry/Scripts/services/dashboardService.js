﻿angular
    .module('homer')
    .factory('dashboardService', dashboardService);

function dashboardService($http) {
    return {
        GetAll: function (id) {
            return $http({
                url: "/Dashboard/GetAll",
                params: { 'id': id },
                dataType: 'json',
                method: 'GET'
            });
        }
    }
}