﻿angular
    .module('homer')
    .factory('designationService', designationService);

function designationService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/Designation/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        }
    }
}