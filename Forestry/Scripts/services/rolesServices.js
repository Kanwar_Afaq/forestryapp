﻿angular
    .module('homer')
    .factory('rolesService', rolesService);

function rolesService($http) {
    return {
        GetAll: function () {
            return $http({
                url: "/Roles/GetAll",
                dataType: 'json',
                method: 'GET'
            });
        }
    }
}