using System; 
using BusinessRepository.BusinessObject.Base; 
 
namespace BusinessRepository.BusinessObject 
{ 
     /// <summary>
     /// This file will not be overwritten.  You can put 
     /// additional Employees Business Layer code in this class. 
     /// </summary>
     public class Employees : EmployeesBase 
     {

        public string Error { get; set; }
        public string FullName { get; set; }
        public string Designation { get; set; }
        public string Location { get; set; }

        public string Key { get; set; }

        // constructor 
        public Employees() 
         {
         } 
     } 
} 
