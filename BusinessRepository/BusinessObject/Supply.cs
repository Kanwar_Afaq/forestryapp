using System;
using BusinessRepository.BusinessObject.Base;
using System.Collections.Generic;

namespace BusinessRepository.BusinessObject 
{ 
     /// <summary>
     /// This file will not be overwritten.  You can put 
     /// additional Supply Business Layer code in this class. 
     /// </summary>
     public class Supply : SupplyBase 
     {
        public List<SupplyItems> supplyItemCollection = new List<SupplyItems>();
        public List<LatitudeLongitude> SupplyCoordinates = new List<LatitudeLongitude>();

        public string RaisingName { get; set; }
        public string SchemeName { get; set; }
        public int? TotalQty { get; set; }
        public DateTime? SuppliedDate { get; set; }

        public string EmpName { get; set; }
        public string ItemName { get; set; }
        public string ItemSize { get; set; }

        // constructor 
        public Supply() 
         { 
         } 
     } 
} 
