using System;
using System.Data;
using BusinessRepository.DataLayer;
using BusinessRepository.BusinessObject;
using BusinessRepository.Models;

namespace BusinessRepository.BusinessObject.Base
{
     /// <summary>
     /// Base class for Employees.  Do not make changes to this class,
     /// instead, put additional code in the Employees class
     /// </summary>
     public class EmployeesBase : BusinessRepository.Models.EmployeesModel
     {

         /// <summary>
         /// Constructor
         /// </summary>
         public EmployeesBase()
         {
         }

         /// <summary>
         /// Selects a record by primary key(s)
         /// </summary>
         public static Employees SelectByPrimaryKey(int id)
         {
             return EmployeesDataLayer.SelectByPrimaryKey(id);
         }

         /// <summary>
         /// Gets the total number of records in the Employees table
         /// </summary>
         public static int GetRecordCount()
         {
             return EmployeesDataLayer.GetRecordCount();
         }

         /// <summary>
         /// Gets the total number of records in the Employees table based on search parameters
         /// </summary>
         public static int GetRecordCountDynamicWhere(int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active)
         {
             return EmployeesDataLayer.GetRecordCountDynamicWhere(id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active);
         }

         /// <summary>
         /// Selects records as a collection (List) of Employees sorted by the sortByExpression and returns the rows (# of records) starting from the startRowIndex
         /// </summary>
         public static EmployeesCollection SelectSkipAndTake(int rows, int startRowIndex, out int totalRowCount, string sortByExpression)
         {
             totalRowCount = GetRecordCount();
             sortByExpression = GetSortExpression(sortByExpression);

             return EmployeesDataLayer.SelectSkipAndTake(sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Selects records as a collection (List) of Employees sorted by the sortByExpression starting from the startRowIndex.
         /// </summary>
         public static EmployeesCollection SelectSkipAndTake(int rows, int startRowIndex, string sortByExpression)
         {
             sortByExpression = GetSortExpression(sortByExpression);
             return EmployeesDataLayer.SelectSkipAndTake(sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Selects records as a collection (List) of Employees sorted by the sortByExpression and returns the rows (# of records) starting from the startRowIndex, based on the search parameters
         /// </summary>
         public static EmployeesCollection SelectSkipAndTakeDynamicWhere(int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active, int rows, int startRowIndex, out int totalRowCount, string sortByExpression)
         {
             totalRowCount = GetRecordCountDynamicWhere(id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active);
             sortByExpression = GetSortExpression(sortByExpression);
             return EmployeesDataLayer.SelectSkipAndTakeDynamicWhere(id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active, sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Selects records as a collection (List) of Employees sorted by the sortByExpression starting from the startRowIndex, based on the search parameters
         /// </summary>
         public static EmployeesCollection SelectSkipAndTakeDynamicWhere(int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active, int rows, int startRowIndex, string sortByExpression)
         {
             sortByExpression = GetSortExpression(sortByExpression);
             return EmployeesDataLayer.SelectSkipAndTakeDynamicWhere(id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active, sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Selects all records as a collection (List) of Employees
         /// </summary>
         public static EmployeesCollection SelectAll()
         {
             return EmployeesDataLayer.SelectAll();
         }

        public static Employees CheckAndGenerateForgetPasswordKey(string userName)
        {
            return EmployeesDataLayer.CheckAndGenerateForgetPasswordKey(userName);
        }

        public static string IsChangePasswordKeyValid(int empID, string key)
        {
            return EmployeesDataLayer.IsChangePasswordKeyValid(empID, key);
        }

        public static EmployeesCollection SelectEmployeesUnderMe(int id)
        {
            return EmployeesDataLayer.SelectEmployeesUnderMe(id);
        }

        /// <summary>
        /// Selects all records as a collection (List) of Employees sorted by the sort expression
        /// </summary>
        public static EmployeesCollection SelectAll(string sortExpression)
         {
             EmployeesCollection objEmployeesCol = EmployeesDataLayer.SelectAll();
             return SortByExpression(objEmployeesCol, sortExpression);
         }

         /// <summary>
         /// Selects records based on the passed filters as a collection (List) of Employees.
         /// </summary>
         public static EmployeesCollection SelectAllDynamicWhere(int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active)
         {
             return EmployeesDataLayer.SelectAllDynamicWhere(id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active);
         }

         /// <summary>
         /// Selects records based on the passed filters as a collection (List) of Employees sorted by the sort expression.
         /// </summary>
         public static EmployeesCollection SelectAllDynamicWhere(int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active, string sortExpression)
         {
             EmployeesCollection objEmployeesCol = EmployeesDataLayer.SelectAllDynamicWhere(id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active);
             return SortByExpression(objEmployeesCol, sortExpression);
         }

         /// <summary>
         /// Selects Id and UserName columns for use with a DropDownList web control, ComboBox, CheckedBoxList, ListView, ListBox, etc
         /// </summary>
         public static EmployeesCollection SelectEmployeesDropDownListData()
         {
             return EmployeesDataLayer.SelectEmployeesDropDownListData();
         }

         /// <summary>
         /// Sorts the EmployeesCollection by sort expression
         /// </summary>
         public static EmployeesCollection SortByExpression(EmployeesCollection objEmployeesCol, string sortExpression)
         {
             bool isSortDescending = sortExpression.ToLower().Contains(" desc");

             if (isSortDescending)
             {
                 sortExpression = sortExpression.Replace(" DESC", "");
                 sortExpression = sortExpression.Replace(" desc", "");
             }
             else
             {
                 sortExpression = sortExpression.Replace(" ASC", "");
                 sortExpression = sortExpression.Replace(" asc", "");
             }

             switch (sortExpression)
             {
                 case "Id":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ById);
                     break;
                 case "UserName":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByUserName);
                     break;
                 case "Password":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByPassword);
                     break;
                 case "FirstName":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByFirstName);
                     break;
                 case "LastName":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByLastName);
                     break;
                 case "DepartmentID":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByDepartmentID);
                     break;
                 case "DesignationID":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByDesignationID);
                     break;
                 case "LocationID":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByLocationID);
                     break;
                 case "RoleID":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByRoleID);
                     break;
                 case "Email":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByEmail);
                     break;
                 case "Address":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByAddress);
                     break;
                 case "ModifiedBy":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByModifiedBy);
                     break;
                 case "ModifiedDate":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByModifiedDate);
                     break;
                 case "Active":
                     objEmployeesCol.Sort(BusinessRepository.BusinessObject.Employees.ByActive);
                     break;
                 default:
                     break;
             }

             if (isSortDescending)
                 objEmployeesCol.Reverse();

             return objEmployeesCol;
         }

         /// <summary>
         /// Inserts a record
         /// </summary>
         public int Insert()
         {
             Employees objEmployees = (Employees)this;
             return EmployeesDataLayer.Insert(objEmployees);
         }

         /// <summary>
         /// Updates a record
         /// </summary>
         public void Update()
         {
             Employees objEmployees = (Employees)this;
             EmployeesDataLayer.Update(objEmployees);
         }

         /// <summary>
         /// Deletes a record based on primary key(s)
         /// </summary>
         public static void Delete(int id)
         {
             EmployeesDataLayer.Delete(id);
         }

         public static bool DoesUserNameExists(string userName)
         {
            return EmployeesDataLayer.DoesUserNameExists(userName);
         }

        public static bool IsPageAllowed(string pageName, int designationID, int roleID)
        {
            return EmployeesDataLayer.IsPageAllowed(pageName, designationID, roleID);
        }

        public static bool ChangePassword(int empID, string password, string oldPassword, bool isForgotPassword)
        {
            return EmployeesDataLayer.ChangePassword(empID, password, oldPassword, isForgotPassword);
        }

        public static bool ActivateDeactivateEmployee(int id)
        {
            return EmployeesDataLayer.ActivateDeactivateEmployee(id);
        }

         public static Employees EmployeeAuthenticate(Employees employeeModel)
         {
            return EmployeesDataLayer.EmployeeAuthenticate(employeeModel);
         } 

         private static string GetSortExpression(string sortByExpression)
         {
             if (String.IsNullOrEmpty(sortByExpression) || sortByExpression == " asc")
                 sortByExpression = "Id";
             else if (sortByExpression.Contains(" asc"))
                 sortByExpression = sortByExpression.Replace(" asc", "");

             return sortByExpression;
         }
        
         /// <summary>
         /// Compares Id used for sorting
         /// </summary>
         public static Comparison<Employees> ById = delegate(Employees x, Employees y)
         {
             return x.Id.CompareTo(y.Id);
         };

         /// <summary>
         /// Compares UserName used for sorting
         /// </summary>
         public static Comparison<Employees> ByUserName = delegate(Employees x, Employees y)
         {
             string value1 = x.UserName ?? String.Empty;
             string value2 = y.UserName ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares Password used for sorting
         /// </summary>
         public static Comparison<Employees> ByPassword = delegate(Employees x, Employees y)
         {
             string value1 = x.Password ?? String.Empty;
             string value2 = y.Password ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares FirstName used for sorting
         /// </summary>
         public static Comparison<Employees> ByFirstName = delegate(Employees x, Employees y)
         {
             string value1 = x.FirstName ?? String.Empty;
             string value2 = y.FirstName ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares LastName used for sorting
         /// </summary>
         public static Comparison<Employees> ByLastName = delegate(Employees x, Employees y)
         {
             string value1 = x.LastName ?? String.Empty;
             string value2 = y.LastName ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares DepartmentID used for sorting
         /// </summary>
         public static Comparison<Employees> ByDepartmentID = delegate(Employees x, Employees y)
         {
             return Nullable.Compare(x.DepartmentID, y.DepartmentID);
         };

         /// <summary>
         /// Compares DesignationID used for sorting
         /// </summary>
         public static Comparison<Employees> ByDesignationID = delegate(Employees x, Employees y)
         {
             return Nullable.Compare(x.DesignationID, y.DesignationID);
         };

         /// <summary>
         /// Compares LocationID used for sorting
         /// </summary>
         public static Comparison<Employees> ByLocationID = delegate(Employees x, Employees y)
         {
             return Nullable.Compare(x.LocationID, y.LocationID);
         };

         /// <summary>
         /// Compares RoleID used for sorting
         /// </summary>
         public static Comparison<Employees> ByRoleID = delegate(Employees x, Employees y)
         {
             return Nullable.Compare(x.RoleID, y.RoleID);
         };

         /// <summary>
         /// Compares Email used for sorting
         /// </summary>
         public static Comparison<Employees> ByEmail = delegate(Employees x, Employees y)
         {
             string value1 = x.Email ?? String.Empty;
             string value2 = y.Email ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares Address used for sorting
         /// </summary>
         public static Comparison<Employees> ByAddress = delegate(Employees x, Employees y)
         {
             string value1 = x.Address ?? String.Empty;
             string value2 = y.Address ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares ModifiedBy used for sorting
         /// </summary>
         public static Comparison<Employees> ByModifiedBy = delegate(Employees x, Employees y)
         {
             return Nullable.Compare(x.ModifiedBy, y.ModifiedBy);
         };

         /// <summary>
         /// Compares ModifiedDate used for sorting
         /// </summary>
         public static Comparison<Employees> ByModifiedDate = delegate(Employees x, Employees y)
         {
             return Nullable.Compare(x.ModifiedDate, y.ModifiedDate);
         };

         /// <summary>
         /// Compares Active used for sorting
         /// </summary>
         public static Comparison<Employees> ByActive = delegate(Employees x, Employees y)
         {
             return x.Active.CompareTo(y.Active);
         };

     }
}
