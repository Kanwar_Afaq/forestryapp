using System;
using System.Data;
using BusinessRepository.DataLayer;
using BusinessRepository.BusinessObject;
using BusinessRepository.Models;

namespace BusinessRepository.BusinessObject.Base
{
     /// <summary>
     /// Base class for Supply.  Do not make changes to this class,
     /// instead, put additional code in the Supply class
     /// </summary>
     public class SupplyBase : BusinessRepository.Models.SupplyModel
     {

         /// <summary>
         /// Constructor
         /// </summary>
         public SupplyBase()
         {
         }

         /// <summary>
         /// Selects a record by primary key(s)
         /// </summary>
         public static Supply SelectByPrimaryKey(int id)
         {
             return SupplyDataLayer.SelectByPrimaryKey(id);
         }

         

         /// <summary>
         /// Gets the total number of records in the Supply table
         /// </summary>
         public static int GetRecordCount()
         {
             return SupplyDataLayer.GetRecordCount();
         }

         /// <summary>
         /// Gets the total number of records in the Supply table based on search parameters
         /// </summary>
         public static int GetRecordCountDynamicWhere(int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy)
         {
             return SupplyDataLayer.GetRecordCountDynamicWhere(id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy);
         }

         /// <summary>
         /// Selects records as a collection (List) of Supply sorted by the sortByExpression and returns the rows (# of records) starting from the startRowIndex
         /// </summary>
         public static SupplyCollection SelectSkipAndTake(int rows, int startRowIndex, out int totalRowCount, string sortByExpression)
         {
             totalRowCount = GetRecordCount();
             sortByExpression = GetSortExpression(sortByExpression);

             return SupplyDataLayer.SelectSkipAndTake(sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Selects records as a collection (List) of Supply sorted by the sortByExpression starting from the startRowIndex.
         /// </summary>
         public static SupplyCollection SelectSkipAndTake(int rows, int startRowIndex, string sortByExpression)
         {
             sortByExpression = GetSortExpression(sortByExpression);
             return SupplyDataLayer.SelectSkipAndTake(sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Selects records as a collection (List) of Supply sorted by the sortByExpression and returns the rows (# of records) starting from the startRowIndex, based on the search parameters
         /// </summary>
         public static SupplyCollection SelectSkipAndTakeDynamicWhere(int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy, int rows, int startRowIndex, out int totalRowCount, string sortByExpression)
         {
             totalRowCount = GetRecordCountDynamicWhere(id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy);
             sortByExpression = GetSortExpression(sortByExpression);
             return SupplyDataLayer.SelectSkipAndTakeDynamicWhere(id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy, sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Selects records as a collection (List) of Supply sorted by the sortByExpression starting from the startRowIndex, based on the search parameters
         /// </summary>
         public static SupplyCollection SelectSkipAndTakeDynamicWhere(int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy, int rows, int startRowIndex, string sortByExpression)
         {
             sortByExpression = GetSortExpression(sortByExpression);
             return SupplyDataLayer.SelectSkipAndTakeDynamicWhere(id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy, sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Gets the grand total or sum of fields with a money or decimal data type.  E.g. XTotal, YTotal
         /// </summary>
         public static Supply SelectTotals()
         {
             return SupplyDataLayer.SelectTotals();
         }

         /// <summary>
         /// Selects all records as a collection (List) of Supply
         /// </summary>
         public static SupplyCollection SelectAll()
         {
             return SupplyDataLayer.SelectAll();
         }

        public static SupplyCollection SelectByEmployee(int id)
        {
            return SupplyDataLayer.SelectByEmployee(id);
        }

        public static SupplyCollection GetSupplyReport(int id)
        {
            return SupplyDataLayer.GetSupplyReport(id);
        }

        /// <summary>
        /// Selects all records as a collection (List) of Supply sorted by the sort expression
        /// </summary>
        public static SupplyCollection SelectAll(string sortExpression)
         {
             SupplyCollection objSupplyCol = SupplyDataLayer.SelectAll();
             return SortByExpression(objSupplyCol, sortExpression);
         }

         /// <summary>
         /// Selects records based on the passed filters as a collection (List) of Supply.
         /// </summary>
         public static SupplyCollection SelectAllDynamicWhere(int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy)
         {
             return SupplyDataLayer.SelectAllDynamicWhere(id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy);
         }

         /// <summary>
         /// Selects records based on the passed filters as a collection (List) of Supply sorted by the sort expression.
         /// </summary>
         public static SupplyCollection SelectAllDynamicWhere(int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy, string sortExpression)
         {
             SupplyCollection objSupplyCol = SupplyDataLayer.SelectAllDynamicWhere(id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy);
             return SortByExpression(objSupplyCol, sortExpression);
         }

         /// <summary>
         /// Selects Id and CustomerName columns for use with a DropDownList web control, ComboBox, CheckedBoxList, ListView, ListBox, etc
         /// </summary>
         public static SupplyCollection SelectSupplyDropDownListData()
         {
             return SupplyDataLayer.SelectSupplyDropDownListData();
         }

         /// <summary>
         /// Sorts the SupplyCollection by sort expression
         /// </summary>
         public static SupplyCollection SortByExpression(SupplyCollection objSupplyCol, string sortExpression)
         {
             bool isSortDescending = sortExpression.ToLower().Contains(" desc");

             if (isSortDescending)
             {
                 sortExpression = sortExpression.Replace(" DESC", "");
                 sortExpression = sortExpression.Replace(" desc", "");
             }
             else
             {
                 sortExpression = sortExpression.Replace(" ASC", "");
                 sortExpression = sortExpression.Replace(" asc", "");
             }

             switch (sortExpression)
             {
                 case "Id":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ById);
                     break;
                 case "RaisingID":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByRaisingID);
                     break;
                 case "SchemeID":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.BySchemeID);
                     break;
                 case "CustomerID":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByCustomerID);
                     break;
                 case "CustomerName":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByCustomerName);
                     break;
                 case "ArmedForcesID":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByArmedForcesID);
                     break;
                 case "Contact":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByContact);
                     break;
                 case "Cnic":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByCnic);
                     break;
                 case "SupplyDate":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.BySupplyDate);
                     break;
                 case "X":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByX);
                     break;
                 case "Y":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByY);
                     break;
                 case "Active":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByActive);
                     break;
                 case "CreatedDate":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByCreatedDate);
                     break;
                 case "CreatedBy":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByCreatedBy);
                     break;
                 case "ModifiedDate":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByModifiedDate);
                     break;
                 case "ModifiedBy":
                     objSupplyCol.Sort(BusinessRepository.BusinessObject.Supply.ByModifiedBy);
                     break;
                 default:
                     break;
             }

             if (isSortDescending)
                 objSupplyCol.Reverse();

             return objSupplyCol;
         }

         /// <summary>
         /// Inserts a record
         /// </summary>
         public int Insert()
         {
             Supply objSupply = (Supply)this;
             return SupplyDataLayer.Insert(objSupply);
         }

         /// <summary>
         /// Updates a record
         /// </summary>
         public void Update()
         {
             Supply objSupply = (Supply)this;
             SupplyDataLayer.Update(objSupply);
         }

         /// <summary>
         /// Deletes a record based on primary key(s)
         /// </summary>
         public static void Delete(int id)
         {
             SupplyDataLayer.Delete(id);
         }

         private static string GetSortExpression(string sortByExpression)
         {
             if (String.IsNullOrEmpty(sortByExpression) || sortByExpression == " asc")
                 sortByExpression = "Id";
             else if (sortByExpression.Contains(" asc"))
                 sortByExpression = sortByExpression.Replace(" asc", "");

             return sortByExpression;
         }

         /// <summary>
         /// Compares Id used for sorting
         /// </summary>
         public static Comparison<Supply> ById = delegate(Supply x, Supply y)
         {
             return x.Id.CompareTo(y.Id);
         };

         /// <summary>
         /// Compares RaisingID used for sorting
         /// </summary>
         public static Comparison<Supply> ByRaisingID = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.RaisingID, y.RaisingID);
         };

         /// <summary>
         /// Compares SchemeID used for sorting
         /// </summary>
         public static Comparison<Supply> BySchemeID = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.SchemeID, y.SchemeID);
         };

         /// <summary>
         /// Compares CustomerID used for sorting
         /// </summary>
         public static Comparison<Supply> ByCustomerID = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.CustomerID, y.CustomerID);
         };

         /// <summary>
         /// Compares CustomerName used for sorting
         /// </summary>
         public static Comparison<Supply> ByCustomerName = delegate(Supply x, Supply y)
         {
             string value1 = x.CustomerName ?? String.Empty;
             string value2 = y.CustomerName ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares ArmedForcesID used for sorting
         /// </summary>
         public static Comparison<Supply> ByArmedForcesID = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.ArmedForcesID, y.ArmedForcesID);
         };

         /// <summary>
         /// Compares Contact used for sorting
         /// </summary>
         public static Comparison<Supply> ByContact = delegate(Supply x, Supply y)
         {
             string value1 = x.Contact ?? String.Empty;
             string value2 = y.Contact ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares Cnic used for sorting
         /// </summary>
         public static Comparison<Supply> ByCnic = delegate(Supply x, Supply y)
         {
             string value1 = x.Cnic ?? String.Empty;
             string value2 = y.Cnic ?? String.Empty;
             return value1.CompareTo(value2);
         };

         /// <summary>
         /// Compares SupplyDate used for sorting
         /// </summary>
         public static Comparison<Supply> BySupplyDate = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.SupplyDate, y.SupplyDate);
         };

         /// <summary>
         /// Compares X used for sorting
         /// </summary>
         public static Comparison<Supply> ByX = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.X, y.X);
         };

         /// <summary>
         /// Compares Y used for sorting
         /// </summary>
         public static Comparison<Supply> ByY = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.Y, y.Y);
         };

         /// <summary>
         /// Compares Active used for sorting
         /// </summary>
         public static Comparison<Supply> ByActive = delegate(Supply x, Supply y)
         {
             return x.Active.CompareTo(y.Active);
         };

         /// <summary>
         /// Compares CreatedDate used for sorting
         /// </summary>
         public static Comparison<Supply> ByCreatedDate = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.CreatedDate, y.CreatedDate);
         };

         /// <summary>
         /// Compares CreatedBy used for sorting
         /// </summary>
         public static Comparison<Supply> ByCreatedBy = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.CreatedBy, y.CreatedBy);
         };

         /// <summary>
         /// Compares ModifiedDate used for sorting
         /// </summary>
         public static Comparison<Supply> ByModifiedDate = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.ModifiedDate, y.ModifiedDate);
         };

         /// <summary>
         /// Compares ModifiedBy used for sorting
         /// </summary>
         public static Comparison<Supply> ByModifiedBy = delegate(Supply x, Supply y)
         {
             return Nullable.Compare(x.ModifiedBy, y.ModifiedBy);
         };

     }
}
