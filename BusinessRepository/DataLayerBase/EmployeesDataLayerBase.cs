using System;
using System.Data;
using System.Data.SqlClient;
using BusinessRepository.BusinessObject;
using System.Configuration;

namespace BusinessRepository.DataLayer.Base
{
    /// <summary>
    /// Base class for EmployeesDataLayer.  Do not make changes to this class,
    /// instead, put additional code in the EmployeesDataLayer class
    /// </summary>
    public class EmployeesDataLayerBase
    {
        // constructor
        public EmployeesDataLayerBase()
        {
        }

        /// <summary>
        /// Selects a record by primary key(s)
        /// </summary>
        public static Employees SelectByPrimaryKey(int id)
        {
            Employees objEmployees = null;
            string storedProcName = "[dbo].[Employees_SelectByPrimaryKey]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // parameters
                    command.Parameters.AddWithValue("@id", id);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objEmployees = CreateEmployeesFromDataRowShared(dt.Rows[0]);
                            }
                        }
                    }
                }
            }

            return objEmployees;
        }

        /// <summary>
        /// Gets the total number of records in the Employees table
        /// </summary>
        public static int GetRecordCount()
        {
            return GetRecordCountShared("[dbo].[Employees_GetRecordCount]", null, null, true, null);
        }

        public static int GetRecordCountShared(string storedProcName = null, string param = null, object paramValue = null, bool isUseStoredProc = true, string dynamicSqlScript = null)
        {
            int recordCount = 0;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                recordCount = (int)dt.Rows[0]["RecordCount"];
                            }
                        }
                    }
                }
            }

            return recordCount;
        }

        /// <summary>
        /// Gets the total number of records in the Employees table based on search parameters
        /// </summary>
        public static int GetRecordCountDynamicWhere(int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active)
        {
            int recordCount = 0;
            string storedProcName = "[dbo].[Employees_GetRecordCountWhereDynamic]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // search parameters
                    AddSearchCommandParamsShared(command, id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                recordCount = (int)dt.Rows[0]["RecordCount"];
                            }
                        }
                    }
                }
            }

            return recordCount;
        }

        /// <summary>
        /// Selects Employees records sorted by the sortByExpression and returns records from the startRowIndex with rows (# of rows)
        /// </summary>
        public static EmployeesCollection SelectSkipAndTake(string sortByExpression, int startRowIndex, int rows)
        {
            return SelectShared("[dbo].[Employees_SelectSkipAndTake]", null, null, true, null, sortByExpression, startRowIndex, rows);
        }

        /// <summary>
        /// Selects Employees records sorted by the sortByExpression and returns records from the startRowIndex with rows (# of records) based on search parameters
        /// </summary>
        public static EmployeesCollection SelectSkipAndTakeDynamicWhere(int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active, string sortByExpression, int startRowIndex, int rows)
        {
            EmployeesCollection objEmployeesCol = null;
            string storedProcName = "[dbo].[Employees_SelectSkipAndTakeWhereDynamic]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // select, skip, take, sort parameters
                    command.Parameters.AddWithValue("@start", startRowIndex);
                    command.Parameters.AddWithValue("@numberOfRows", rows);
                    command.Parameters.AddWithValue("@sortByExpression", sortByExpression);

                    // search parameters
                    AddSearchCommandParamsShared(command, id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objEmployeesCol = new EmployeesCollection();

                                foreach (DataRow dr in dt.Rows)
                                {
                                    Employees objEmployees = CreateEmployeesFromDataRowShared(dr);
                                    objEmployeesCol.Add(objEmployees);
                                }
                            }
                        }
                    }
                }
            }

            return objEmployeesCol;
        }

        /// <summary>
        /// Selects all Employees
        /// </summary>
        public static EmployeesCollection SelectAll()
        {
            return SelectShared("[dbo].[Employees_SelectAll]", String.Empty, null);
        }

        public static Employees CheckAndGenerateForgetPasswordKey(string userName)
        {
            Employees objEmployee = null;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("[dbo].[Employees_CheckAndGenerateForgetPasswordKey]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserName", userName);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objEmployee = new Employees();

                                if (dt.Rows[0]["Email"] != DBNull.Value)
                                    objEmployee.Email = (string)dt.Rows[0]["Email"];
                                else
                                    objEmployee.Email = "";

                                if (dt.Rows[0]["Key"] != DBNull.Value)
                                    objEmployee.Key = (string)dt.Rows[0]["Key"];
                                else
                                    objEmployee.Key = "";

                                if (dt.Rows[0]["EmpID"] != DBNull.Value)
                                    objEmployee.Id = (int)dt.Rows[0]["EmpID"];
                                else
                                    objEmployee.Id = 0;
                            }
                        }
                    }
                }
            }

            return objEmployee;
        }

        public static string IsChangePasswordKeyValid(int empID, string key)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("[dbo].[Employees_IsChangePasswordKeyValid]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@EmpID", empID);
                    command.Parameters.AddWithValue("@Key", key);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                return dt.Rows[0]["Error"].ToString();
                            }
                        }
                    }
                }
            }
            return "";
        }

        public static EmployeesCollection SelectEmployeesUnderMe(int id)
        {
            EmployeesCollection objEmployeesCol = null;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("[dbo].[Employees_UnderMe]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    
                        command.Parameters.AddWithValue("@EmpID", id);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objEmployeesCol = new EmployeesCollection();

                                foreach (DataRow dr in dt.Rows)
                                {
                                    Employees objEmployees = CreateEmployeesFromDataRowShared(dr);
                                    objEmployeesCol.Add(objEmployees);
                                }
                            }
                        }
                    }
                }
            }

            return objEmployeesCol;
        }

        /// <summary>
        /// Selects records based on the passed filters as a collection (List) of Employees.
        /// </summary>
        public static EmployeesCollection SelectAllDynamicWhere(int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active)
        {
            EmployeesCollection objEmployeesCol = null;
            string storedProcName = "[dbo].[Employees_SelectAllWhereDynamic]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // search parameters
                    AddSearchCommandParamsShared(command, id, userName, password, firstName, lastName, departmentID, designationID, locationID, roleID, email, address, modifiedBy, modifiedDate, active);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objEmployeesCol = new EmployeesCollection();

                                foreach (DataRow dr in dt.Rows)
                                {
                                    Employees objEmployees = CreateEmployeesFromDataRowShared(dr);
                                    objEmployeesCol.Add(objEmployees);
                                }
                            }
                        }
                    }
                }
            }

            return objEmployeesCol;
        }

        /// <summary>
        /// Selects Id and UserName columns for use with a DropDownList web control
        /// </summary>
        public static EmployeesCollection SelectEmployeesDropDownListData()
        {
            EmployeesCollection objEmployeesCol = null;
            string storedProcName = "[dbo].[Employees_SelectDropDownListData]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objEmployeesCol = new EmployeesCollection();

                                foreach (DataRow dr in dt.Rows)
                                {
                                    Employees objEmployees = new Employees();
                                    objEmployees.Id = (int)dr["Id"];
                                    objEmployees.UserName = (string)(dr["UserName"]);

                                    objEmployeesCol.Add(objEmployees);
                                }
                            }
                        }
                    }
                }
            }

            return objEmployeesCol;
        }

        public static EmployeesCollection SelectShared(string storedProcName, string param, object paramValue, bool isUseStoredProc = true, string dynamicSqlScript = null, string sortByExpression = null, int? startRowIndex = null, int? rows = null)
        {
            EmployeesCollection objEmployeesCol = null;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // select, skip, take, sort parameters
                    if (!String.IsNullOrEmpty(sortByExpression) && startRowIndex != null && rows != null)
                    {
                        command.Parameters.AddWithValue("@start", startRowIndex.Value);
                        command.Parameters.AddWithValue("@numberOfRows", rows.Value);
                        command.Parameters.AddWithValue("@sortByExpression", sortByExpression);
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objEmployeesCol = new EmployeesCollection();

                                foreach (DataRow dr in dt.Rows)
                                {
                                    Employees objEmployees = CreateEmployeesFromDataRowShared(dr);
                                    objEmployeesCol.Add(objEmployees);
                                }
                            }
                        }
                    }
                }
            }

            return objEmployeesCol;
        }

        /// <summary>
        /// Inserts a record
        /// </summary>
        public static int Insert(Employees objEmployees)
        {
            string storedProcName = "[dbo].[Employees_Insert]";
            return InsertUpdate(objEmployees, false, storedProcName);
        }

        /// <summary>
        /// Updates a record
        /// </summary>
        public static void Update(Employees objEmployees)
        {
            string storedProcName = "[dbo].[Employees_Update]";
            InsertUpdate(objEmployees, true, storedProcName);
        }

        private static int InsertUpdate(Employees objEmployees, bool isUpdate, string storedProcName)
        {
            int newlyCreatedId = objEmployees.Id;

            object lastName = objEmployees.LastName;
            object departmentID = objEmployees.DepartmentID;
            object designationID = objEmployees.DesignationID;
            object locationID = objEmployees.LocationID;
            object roleID = objEmployees.RoleID;
            object email = objEmployees.Email;
            object address = objEmployees.Address;
            object modifiedBy = objEmployees.ModifiedBy;
            object modifiedDate = objEmployees.ModifiedDate;

            if (String.IsNullOrEmpty(objEmployees.LastName))
                lastName = System.DBNull.Value;

            if (objEmployees.DepartmentID == null)
                departmentID = System.DBNull.Value;

            if (objEmployees.DesignationID == null)
                designationID = System.DBNull.Value;

            if (objEmployees.LocationID == null)
                locationID = System.DBNull.Value;

            if (objEmployees.RoleID == null)
                roleID = System.DBNull.Value;

            if (String.IsNullOrEmpty(objEmployees.Email))
                email = System.DBNull.Value;

            if (String.IsNullOrEmpty(objEmployees.Address))
                address = System.DBNull.Value;

            if (objEmployees.ModifiedBy == null)
                modifiedBy = System.DBNull.Value;

            if (objEmployees.ModifiedDate == null)
                modifiedDate = System.DBNull.Value;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // parameters
                    if (isUpdate)
                    {
                        // for update only
                        command.Parameters.AddWithValue("@id", objEmployees.Id);
                    }

                    command.Parameters.AddWithValue("@userName", objEmployees.UserName);
                    command.Parameters.AddWithValue("@password", objEmployees.Password);
                    command.Parameters.AddWithValue("@firstName", objEmployees.FirstName);
                    command.Parameters.AddWithValue("@lastName", lastName);
                    command.Parameters.AddWithValue("@departmentID", departmentID);
                    command.Parameters.AddWithValue("@designationID", designationID);
                    command.Parameters.AddWithValue("@locationID", locationID);
                    command.Parameters.AddWithValue("@roleID", roleID);
                    command.Parameters.AddWithValue("@email", email);
                    command.Parameters.AddWithValue("@address", address);
                    command.Parameters.AddWithValue("@modifiedBy", modifiedBy);
                    command.Parameters.AddWithValue("@modifiedDate", modifiedDate);
                    command.Parameters.AddWithValue("@active", objEmployees.Active);

                    if (isUpdate)
                        command.ExecuteNonQuery();
                    else
                        newlyCreatedId = (int)command.ExecuteScalar();
                }
            }

            return newlyCreatedId;
        }

        /// <summary>
        /// Deletes a record based on primary key(s)
        /// </summary>
        public static void Delete(int id)
        {
            string storedProcName = "[dbo].[Employees_Delete]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // parameters
                    command.Parameters.AddWithValue("@id", id);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
        }

        public static bool DoesUserNameExists(string userName)
        {
            string storedProcName = "[dbo].[Employees_UserNameExists]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // parameters
                    command.Parameters.AddWithValue("@UserName", userName);

                    // execute
                    return ((int)command.ExecuteScalar() == 1 ? true : false);
                }
            }
        }

        public static bool IsPageAllowed(string pageName, int designationID, int roleID)
        {
            string storedProcName = "[dbo].[Employees_IsPageAllowed]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // parameters
                    command.Parameters.AddWithValue("@PageName", pageName);
                    command.Parameters.AddWithValue("@DesignationID", designationID);
                    command.Parameters.AddWithValue("@RoleID", roleID);

                    // execute
                    return ((int)command.ExecuteScalar() == 1 ? true : false);
                }
            }
        }

            public static bool ChangePassword(int empID, string password, string oldPassword, bool isForgotPassword)
        {
            string storedProcName = "[dbo].[Employees_ChangePassword]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // parameters
                    command.Parameters.AddWithValue("@EmpID", empID);
                    command.Parameters.AddWithValue("@Password", password);
                    command.Parameters.AddWithValue("@OldPassword", oldPassword);
                    command.Parameters.AddWithValue("@IsForgetPassword", isForgotPassword);

                    // execute
                    return ((int)command.ExecuteScalar() == 1 ? true : false);
                }
            }
        }

        public static bool ActivateDeactivateEmployee(int id)
        {
            string storedProcName = "[dbo].[Employees_ActivateDeactivate]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // parameters
                    command.Parameters.AddWithValue("@ID", id);

                    // execute
                    return ((int)command.ExecuteScalar() == 1 ? true : false);
                }
            }
        }

        public static Employees EmployeeAuthenticate(Employees employeeModel)
        {
            Employees objEmployees = null;
            string storedProcName = "[dbo].[Employees_Authenticate]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // parameters
                    command.Parameters.AddWithValue("@UserName", employeeModel.UserName);
                    command.Parameters.AddWithValue("@Password", employeeModel.Password);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objEmployees = CreateLoginEmployeesFromDataRowShared(dt.Rows[0]);
                            }
                        }
                    }
                }
            }

            return objEmployees;
        }

        /// <summary>
        /// Adds search parameters to the Command object
        /// </summary>
        private static void AddSearchCommandParamsShared(SqlCommand command, int? id, string userName, string password, string firstName, string lastName, int? departmentID, int? designationID, int? locationID, int? roleID, string email, string address, int? modifiedBy, DateTime? modifiedDate, bool? active)
        {
            if (id != null)
                command.Parameters.AddWithValue("@id", id);
            else
                command.Parameters.AddWithValue("@id", System.DBNull.Value);

            if (!String.IsNullOrEmpty(userName))
                command.Parameters.AddWithValue("@userName", userName);
            else
                command.Parameters.AddWithValue("@userName", System.DBNull.Value);

            if (!String.IsNullOrEmpty(password))
                command.Parameters.AddWithValue("@password", password);
            else
                command.Parameters.AddWithValue("@password", System.DBNull.Value);

            if (!String.IsNullOrEmpty(firstName))
                command.Parameters.AddWithValue("@firstName", firstName);
            else
                command.Parameters.AddWithValue("@firstName", System.DBNull.Value);

            if (!String.IsNullOrEmpty(lastName))
                command.Parameters.AddWithValue("@lastName", lastName);
            else
                command.Parameters.AddWithValue("@lastName", System.DBNull.Value);

            if (departmentID != null)
                command.Parameters.AddWithValue("@departmentID", departmentID);
            else
                command.Parameters.AddWithValue("@departmentID", System.DBNull.Value);

            if (designationID != null)
                command.Parameters.AddWithValue("@designationID", designationID);
            else
                command.Parameters.AddWithValue("@designationID", System.DBNull.Value);

            if (locationID != null)
                command.Parameters.AddWithValue("@locationID", locationID);
            else
                command.Parameters.AddWithValue("@locationID", System.DBNull.Value);

            if (roleID != null)
                command.Parameters.AddWithValue("@roleID", roleID);
            else
                command.Parameters.AddWithValue("@roleID", System.DBNull.Value);

            if (!String.IsNullOrEmpty(email))
                command.Parameters.AddWithValue("@email", email);
            else
                command.Parameters.AddWithValue("@email", System.DBNull.Value);

            if (!String.IsNullOrEmpty(address))
                command.Parameters.AddWithValue("@address", address);
            else
                command.Parameters.AddWithValue("@address", System.DBNull.Value);

            if (modifiedBy != null)
                command.Parameters.AddWithValue("@modifiedBy", modifiedBy);
            else
                command.Parameters.AddWithValue("@modifiedBy", System.DBNull.Value);

            if (modifiedDate != null)
                command.Parameters.AddWithValue("@modifiedDate", modifiedDate);
            else
                command.Parameters.AddWithValue("@modifiedDate", System.DBNull.Value);

            if (active != null)
                command.Parameters.AddWithValue("@active", active);
            else
                command.Parameters.AddWithValue("@active", System.DBNull.Value);

        }

        /// <summary>
        /// Creates a Employees object from the passed data row
        /// </summary>
        private static Employees CreateEmployeesFromDataRowShared(DataRow dr)
        {
            Employees objEmployees = new Employees();

            if (dr["Id"] != System.DBNull.Value)
                objEmployees.Id = (int)dr["Id"];
            else
                objEmployees.Id = 0;

            if (dr["UserName"] != System.DBNull.Value)
                objEmployees.UserName = dr["UserName"].ToString();
            else
                objEmployees.UserName = null;

            if (dr["Password"] != System.DBNull.Value)
                objEmployees.Password = dr["Password"].ToString();
            else
                objEmployees.Password = null;

            if (dr["FirstName"] != System.DBNull.Value)
                objEmployees.FirstName = dr["FirstName"].ToString();
            else
                objEmployees.FirstName = null;

            if (dr["LastName"] != System.DBNull.Value)
                objEmployees.LastName = dr["LastName"].ToString();
            else
                objEmployees.LastName = null;

            if (dr["DepartmentID"] != System.DBNull.Value)
                objEmployees.DepartmentID = (int)dr["DepartmentID"];
            else
                objEmployees.DepartmentID = null;

            if (dr["DesignationID"] != System.DBNull.Value)
                objEmployees.DesignationID = (int)dr["DesignationID"];
            else
                objEmployees.DesignationID = null;

            if (dr["LocationID"] != System.DBNull.Value)
                objEmployees.LocationID = (int)dr["LocationID"];
            else
                objEmployees.LocationID = null;

            if (dr["RoleID"] != System.DBNull.Value)
                objEmployees.RoleID = (int)dr["RoleID"];
            else
                objEmployees.RoleID = null;

            if (dr["Email"] != System.DBNull.Value)
                objEmployees.Email = dr["Email"].ToString();
            else
                objEmployees.Email = null;

            if (dr["Address"] != System.DBNull.Value)
                objEmployees.Address = dr["Address"].ToString();
            else
                objEmployees.Address = null;

            if (dr["ModifiedBy"] != System.DBNull.Value)
                objEmployees.ModifiedBy = (int)dr["ModifiedBy"];
            else
                objEmployees.ModifiedBy = null;

            if (dr["ModifiedDate"] != System.DBNull.Value)
                objEmployees.ModifiedDate = (DateTime)dr["ModifiedDate"];
            else
                objEmployees.ModifiedDate = null;
            if (dr["Active"] != System.DBNull.Value)
                objEmployees.Active = (bool)dr["Active"];
            else
                objEmployees.Active = false;

            return objEmployees;
        }

        private static Employees CreateLoginEmployeesFromDataRowShared(DataRow dr)
        {
            Employees objEmployees = new Employees();

            if (dr["ID"] != System.DBNull.Value)
                objEmployees.Id = (int)dr["ID"];
            else
                objEmployees.Id = 0;

            if (dr["UserName"] != System.DBNull.Value)
                objEmployees.UserName = dr["UserName"].ToString();
            else
                objEmployees.UserName = null;
            
            if (dr["FirstName"] != System.DBNull.Value)
                objEmployees.FirstName = dr["FirstName"].ToString();
            else
                objEmployees.FirstName = null;

            if (dr["LastName"] != System.DBNull.Value)
                objEmployees.LastName = dr["LastName"].ToString();
            else
                objEmployees.LastName = null;

            if (dr["DesignationID"] != System.DBNull.Value)
                objEmployees.DesignationID = (int)dr["DesignationID"];
            else
                objEmployees.DesignationID = null;

            if (dr["LocationID"] != System.DBNull.Value)
                objEmployees.LocationID = (int)dr["LocationID"];
            else
                objEmployees.LocationID = null;

            if (dr["Email"] != System.DBNull.Value)
                objEmployees.Email = dr["Email"].ToString();
            else
                objEmployees.Email = null;

            if (dr["RoleID"] != System.DBNull.Value)
                objEmployees.RoleID = (int)dr["RoleID"];
            else
                objEmployees.RoleID = null;

            if (dr["DesignationID"] != System.DBNull.Value)
                objEmployees.DesignationID = (int)dr["DesignationID"];
            else
                objEmployees.DesignationID = null;

            if (dr["Address"] != System.DBNull.Value)
                objEmployees.Address = dr["Address"].ToString();
            else
                objEmployees.Address = null;

            if (dr["Active"] != System.DBNull.Value)
                objEmployees.Active = (int)dr["Active"] == 0? false : true;
            else
                objEmployees.Active = false;

            if (dr["Error"] != System.DBNull.Value)
                objEmployees.Error = (string)dr["Error"];
            else
                objEmployees.Error = null;

            return objEmployees;
        }
    }
}
