using System;
using System.Data;
using System.Data.SqlClient;
using BusinessRepository.BusinessObject;
using System.Configuration;

namespace BusinessRepository.DataLayer.Base
{
     /// <summary>
     /// Base class for SupplyDataLayer.  Do not make changes to this class,
     /// instead, put additional code in the SupplyDataLayer class
     /// </summary>
     public class SupplyDataLayerBase
     {
         // constructor
         public SupplyDataLayerBase()
         {
         }

         /// <summary>
         /// Selects a record by primary key(s)
         /// </summary>
         public static Supply SelectByPrimaryKey(int id)
         {
              Supply objSupply = null;
              string storedProcName = "[dbo].[Supply_SelectByPrimaryKey]";

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      // parameters
                      command.Parameters.AddWithValue("@id", id);

                      using (SqlDataAdapter da = new SqlDataAdapter(command))
                      {
                          DataTable dt = new DataTable();
                          da.Fill(dt);

                          if (dt != null)
                          {
                              if (dt.Rows.Count > 0)
                              {
                                  objSupply = CreateSupplyFromDataRowShared(dt.Rows[0]);
                              }
                          }
                      }
                  }
              }

              return objSupply;
         }

       
        /// <summary>
        /// Gets the total number of records in the Supply table
        /// </summary>
        public static int GetRecordCount()
         {
             return GetRecordCountShared("[dbo].[Supply_GetRecordCount]", null, null, true, null);
         }

         public static int GetRecordCountShared(string storedProcName = null, string param = null, object paramValue = null, bool isUseStoredProc = true, string dynamicSqlScript = null)
         {
              int recordCount = 0;

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      using (SqlDataAdapter da = new SqlDataAdapter(command))
                      {
                          DataTable dt = new DataTable();
                          da.Fill(dt);

                          if (dt != null)
                          {
                              if (dt.Rows.Count > 0)
                              {
                                  recordCount = (int)dt.Rows[0]["RecordCount"];
                              }
                          }
                      }
                  }
              }

              return recordCount;
         }

         /// <summary>
         /// Gets the total number of records in the Supply table based on search parameters
         /// </summary>
         public static int GetRecordCountDynamicWhere(int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy)
         {
              int recordCount = 0;
              string storedProcName = "[dbo].[Supply_GetRecordCountWhereDynamic]";

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      // search parameters
                      AddSearchCommandParamsShared(command, id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy);

                      using (SqlDataAdapter da = new SqlDataAdapter(command))
                      {
                          DataTable dt = new DataTable();
                          da.Fill(dt);

                          if (dt != null)
                          {
                              if (dt.Rows.Count > 0)
                              {
                                  recordCount = (int)dt.Rows[0]["RecordCount"];
                              }
                          }
                      }
                  }
              }

              return recordCount;
         }

         /// <summary>
         /// Selects Supply records sorted by the sortByExpression and returns records from the startRowIndex with rows (# of rows)
         /// </summary>
         public static SupplyCollection SelectSkipAndTake(string sortByExpression, int startRowIndex, int rows)
         {
             return SelectShared("[dbo].[Supply_SelectSkipAndTake]", null, null, true, null, sortByExpression, startRowIndex, rows);
         }

         /// <summary>
         /// Selects Supply records sorted by the sortByExpression and returns records from the startRowIndex with rows (# of records) based on search parameters
         /// </summary>
         public static SupplyCollection SelectSkipAndTakeDynamicWhere(int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy, string sortByExpression, int startRowIndex, int rows)
         {
              SupplyCollection objSupplyCol = null;
              string storedProcName = "[dbo].[Supply_SelectSkipAndTakeWhereDynamic]";

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      // select, skip, take, sort parameters
                      command.Parameters.AddWithValue("@start", startRowIndex);
                      command.Parameters.AddWithValue("@numberOfRows", rows);
                      command.Parameters.AddWithValue("@sortByExpression", sortByExpression);

                      // search parameters
                      AddSearchCommandParamsShared(command, id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy);

                      using (SqlDataAdapter da = new SqlDataAdapter(command))
                      {
                          DataTable dt = new DataTable();
                          da.Fill(dt);

                          if (dt != null)
                          {
                              if (dt.Rows.Count > 0)
                              {
                                  objSupplyCol = new SupplyCollection();

                                  foreach (DataRow dr in dt.Rows)
                                  {
                                      Supply objSupply = CreateSupplyFromDataRowShared(dr);
                                      objSupplyCol.Add(objSupply);
                                  }
                              }
                          }
                      }
                  }
              }

              return objSupplyCol;
         }

         /// <summary>
         /// Gets the grand total or sum of fields with a money of decimal data type
         /// </summary>
         public static Supply SelectTotals()
         {
              Supply objSupply = null;
              string storedProcName = "[dbo].[Supply_SelectTotals]";

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      using (SqlDataAdapter da = new SqlDataAdapter(command))
                      {
                          DataTable dt = new DataTable();
                          da.Fill(dt);

                          if (dt != null)
                          {
                              if (dt.Rows.Count > 0)
                              {
                                  if(dt.Rows[0]["XTotal"] != DBNull.Value)
                                       objSupply.XTotal = (decimal)dt.Rows[0]["XTotal"];
                                  if(dt.Rows[0]["YTotal"] != DBNull.Value)
                                       objSupply.YTotal = (decimal)dt.Rows[0]["YTotal"];
                              }
                          }
                      }
                  }
              }

              return objSupply;
         }

         /// <summary>
         /// Selects all Supply
         /// </summary>
         public static SupplyCollection SelectAll()
         {
             return SelectShared("[dbo].[Supply_SelectAll]", String.Empty, null);
         }

        public static SupplyCollection SelectByEmployee(int id)
        {
            SupplyCollection objSupplyCol = null;
            string storedProcName = "[dbo].[Supply_SelectAllByEmployee]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@EmpID", id);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objSupplyCol = new SupplyCollection();

                                foreach (DataRow dr in dt.Rows)
                                {
                                    Supply objSupply = CreateSupplyFromDataRowSharedCustom(dr);
                                    objSupplyCol.Add(objSupply);
                                }
                            }
                        }
                    }
                }
            }

            return objSupplyCol;
        }

        public static SupplyCollection GetSupplyReport(int id)
        {
            SupplyCollection objSupplyCol = null;
            string storedProcName = "[dbo].[Supply_Report]";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(storedProcName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@EmpID", id);

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objSupplyCol = new SupplyCollection();

                                foreach (DataRow dr in dt.Rows)
                                {
                                    Supply objSupply = CreateSupplyFromDataRowSharedCustom(dr);

                                    if (dr["EmpName"] != System.DBNull.Value)
                                        objSupply.EmpName = (string)dr["EmpName"];
                                    else
                                        objSupply.EmpName = null;

                                    if (dr["ItemName"] != System.DBNull.Value)
                                        objSupply.ItemName = (string)dr["ItemName"];
                                    else
                                        objSupply.ItemName = null;

                                    if (dr["ItemSize"] != System.DBNull.Value)
                                        objSupply.ItemSize = (string)dr["ItemSize"];
                                    else
                                        objSupply.ItemSize = null;

                                    objSupplyCol.Add(objSupply);
                                }
                            }
                        }
                    }
                }
            }

            return objSupplyCol;
        }

        /// <summary>
        /// Selects records based on the passed filters as a collection (List) of Supply.
        /// </summary>
        public static SupplyCollection SelectAllDynamicWhere(int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy)
         {
              SupplyCollection objSupplyCol = null;
              string storedProcName = "[dbo].[Supply_SelectAllWhereDynamic]";

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      // search parameters
                      AddSearchCommandParamsShared(command, id, raisingID, schemeID, customerID, customerName, armedForcesID, contact, cnic, supplyDate, X, Y, active, createdDate, createdBy, modifiedDate, modifiedBy);

                      using (SqlDataAdapter da = new SqlDataAdapter(command))
                      {
                          DataTable dt = new DataTable();
                          da.Fill(dt);

                          if (dt != null)
                          {
                              if (dt.Rows.Count > 0)
                              {
                                  objSupplyCol = new SupplyCollection();

                                  foreach (DataRow dr in dt.Rows)
                                  {
                                      Supply objSupply = CreateSupplyFromDataRowShared(dr);
                                      objSupplyCol.Add(objSupply);
                                  }
                              }
                          }
                      }
                  }
              }

              return objSupplyCol;
         }

         /// <summary>
         /// Selects Id and CustomerName columns for use with a DropDownList web control
         /// </summary>
         public static SupplyCollection SelectSupplyDropDownListData()
         {
              SupplyCollection objSupplyCol = null;
              string storedProcName = "[dbo].[Supply_SelectDropDownListData]";

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      using (SqlDataAdapter da = new SqlDataAdapter(command))
                      {
                          DataTable dt = new DataTable();
                          da.Fill(dt);

                          if (dt != null)
                          {
                              if (dt.Rows.Count > 0)
                              {
                                  objSupplyCol = new SupplyCollection();

                                  foreach (DataRow dr in dt.Rows)
                                  {
                                      Supply objSupply = new Supply();
                                      objSupply.Id = (int)dr["Id"];

                                      if (dr["CustomerName"] != System.DBNull.Value)
                                          objSupply.CustomerName = (string)(dr["CustomerName"]);
                                      else
                                          objSupply.CustomerName = null;

                                      objSupplyCol.Add(objSupply);
                                  }
                              }
                          }
                      }
                  }
              }

              return objSupplyCol;
         }

         public static SupplyCollection SelectShared(string storedProcName, string param, object paramValue, bool isUseStoredProc = true, string dynamicSqlScript = null, string sortByExpression = null, int? startRowIndex = null, int? rows = null)
         {
              SupplyCollection objSupplyCol = null;

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      // select, skip, take, sort parameters
                      if (!String.IsNullOrEmpty(sortByExpression) && startRowIndex != null && rows != null)
                      {
                          command.Parameters.AddWithValue("@start", startRowIndex.Value);
                          command.Parameters.AddWithValue("@numberOfRows", rows.Value);
                          command.Parameters.AddWithValue("@sortByExpression", sortByExpression);
                      }

                      using (SqlDataAdapter da = new SqlDataAdapter(command))
                      {
                          DataTable dt = new DataTable();
                          da.Fill(dt);

                          if (dt != null)
                          {
                              if (dt.Rows.Count > 0)
                              {
                                  objSupplyCol = new SupplyCollection();

                                  foreach (DataRow dr in dt.Rows)
                                  {
                                      Supply objSupply = CreateSupplyFromDataRowShared(dr);
                                      objSupplyCol.Add(objSupply);
                                  }
                              }
                          }
                      }
                  }
              }

              return objSupplyCol;
         }

         /// <summary>
         /// Inserts a record
         /// </summary>
         public static int Insert(Supply objSupply)
         {
             string storedProcName = "[dbo].[Supply_Insert]";
             return InsertUpdate(objSupply, false, storedProcName);
         }

         /// <summary>
         /// Updates a record
         /// </summary>
         public static void Update(Supply objSupply)
         {
             string storedProcName = "[dbo].[Supply_Update]";
             InsertUpdate(objSupply, true, storedProcName);
         }

         private static int InsertUpdate(Supply objSupply, bool isUpdate, string storedProcName)
         {
              int newlyCreatedId = objSupply.Id;

              object empID = objSupply.EmpID;
              object raisingID = objSupply.RaisingID;
              object schemeID = objSupply.SchemeID;
              object customerID = objSupply.CustomerID;
              object customerName = objSupply.CustomerName;
              object armedForcesID = objSupply.ArmedForcesID;
              object contact = objSupply.Contact;
              object cnic = objSupply.Cnic;
              object supplyDate = objSupply.SupplyDate;
              object X = objSupply.X;
              object Y = objSupply.Y;
              object createdDate = objSupply.CreatedDate;
              object createdBy = objSupply.CreatedBy;
              object modifiedDate = objSupply.ModifiedDate;
              object modifiedBy = objSupply.ModifiedBy;

            if (objSupply.EmpID == null)
                empID = System.DBNull.Value;

            if (objSupply.RaisingID == null)
                  raisingID = System.DBNull.Value;

              if (objSupply.SchemeID == null)
                  schemeID = System.DBNull.Value;

              if (objSupply.CustomerID == null)
                  customerID = System.DBNull.Value;

              if (String.IsNullOrEmpty(objSupply.CustomerName))
                  customerName = System.DBNull.Value;

              if (objSupply.ArmedForcesID == null)
                  armedForcesID = System.DBNull.Value;

              if (String.IsNullOrEmpty(objSupply.Contact))
                  contact = System.DBNull.Value;

              if (String.IsNullOrEmpty(objSupply.Cnic))
                  cnic = System.DBNull.Value;

              if (objSupply.SupplyDate == null)
                  supplyDate = System.DBNull.Value;

              if (objSupply.X == null)
                  X = System.DBNull.Value;

              if (objSupply.Y == null)
                  Y = System.DBNull.Value;

              if (objSupply.CreatedDate == null)
                  createdDate = System.DBNull.Value;

              if (objSupply.CreatedBy == null)
                  createdBy = System.DBNull.Value;

              if (objSupply.ModifiedDate == null)
                  modifiedDate = System.DBNull.Value;

              if (objSupply.ModifiedBy == null)
                  modifiedBy = System.DBNull.Value;

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      // parameters
                      if (isUpdate)
                      {
                          // for update only
                          command.Parameters.AddWithValue("@id", objSupply.Id);
                      }

                      command.Parameters.AddWithValue("@empID", empID);
                      command.Parameters.AddWithValue("@raisingID", raisingID);
                      command.Parameters.AddWithValue("@schemeID", schemeID);
                      command.Parameters.AddWithValue("@customerID", customerID);
                      command.Parameters.AddWithValue("@customerName", customerName);
                      command.Parameters.AddWithValue("@armedForcesID", armedForcesID);
                      command.Parameters.AddWithValue("@contact", contact);
                      command.Parameters.AddWithValue("@cnic", cnic);
                      command.Parameters.AddWithValue("@supplyDate", supplyDate);
                      command.Parameters.AddWithValue("@X", X);
                      command.Parameters.AddWithValue("@Y", Y);
                      command.Parameters.AddWithValue("@active", objSupply.Active);
                      command.Parameters.AddWithValue("@createdDate", createdDate);
                      command.Parameters.AddWithValue("@createdBy", createdBy);
                      command.Parameters.AddWithValue("@modifiedDate", modifiedDate);
                      command.Parameters.AddWithValue("@modifiedBy", modifiedBy);

                      if (isUpdate)
                          command.ExecuteNonQuery();
                      else
                          newlyCreatedId = (int)command.ExecuteScalar();
                  }
              }

              return newlyCreatedId;
         }

         /// <summary>
         /// Deletes a record based on primary key(s)
         /// </summary>
         public static void Delete(int id)
         {
              string storedProcName = "[dbo].[Supply_Delete]";

              using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ForestryCS"].ConnectionString))
              {
                  connection.Open();

                  using (SqlCommand command = new SqlCommand(storedProcName, connection))
                  {
                      command.CommandType = CommandType.StoredProcedure;

                      // parameters
                      command.Parameters.AddWithValue("@id", id);

                      // execute
                      command.ExecuteNonQuery();
                  }
              }
         }

         /// <summary>
         /// Adds search parameters to the Command object
         /// </summary>
         private static void AddSearchCommandParamsShared(SqlCommand command, int? id, int? raisingID, int? schemeID, int? customerID, string customerName, int? armedForcesID, string contact, string cnic, DateTime? supplyDate, decimal? X, decimal? Y, bool? active, DateTime? createdDate, int? createdBy, DateTime? modifiedDate, int? modifiedBy)
         {
              if(id != null)
                  command.Parameters.AddWithValue("@id", id);
              else
                  command.Parameters.AddWithValue("@id", System.DBNull.Value);

              if(raisingID != null)
                  command.Parameters.AddWithValue("@raisingID", raisingID);
              else
                  command.Parameters.AddWithValue("@raisingID", System.DBNull.Value);

              if(schemeID != null)
                  command.Parameters.AddWithValue("@schemeID", schemeID);
              else
                  command.Parameters.AddWithValue("@schemeID", System.DBNull.Value);

              if(customerID != null)
                  command.Parameters.AddWithValue("@customerID", customerID);
              else
                  command.Parameters.AddWithValue("@customerID", System.DBNull.Value);

              if(!String.IsNullOrEmpty(customerName))
                  command.Parameters.AddWithValue("@customerName", customerName);
              else
                  command.Parameters.AddWithValue("@customerName", System.DBNull.Value);

              if(armedForcesID != null)
                  command.Parameters.AddWithValue("@armedForcesID", armedForcesID);
              else
                  command.Parameters.AddWithValue("@armedForcesID", System.DBNull.Value);

              if(!String.IsNullOrEmpty(contact))
                  command.Parameters.AddWithValue("@contact", contact);
              else
                  command.Parameters.AddWithValue("@contact", System.DBNull.Value);

              if(!String.IsNullOrEmpty(cnic))
                  command.Parameters.AddWithValue("@cnic", cnic);
              else
                  command.Parameters.AddWithValue("@cnic", System.DBNull.Value);

              if(supplyDate != null)
                  command.Parameters.AddWithValue("@supplyDate", supplyDate);
              else
                  command.Parameters.AddWithValue("@supplyDate", System.DBNull.Value);

              if(X != null)
                  command.Parameters.AddWithValue("@X", X);
              else
                  command.Parameters.AddWithValue("@X", System.DBNull.Value);

              if(Y != null)
                  command.Parameters.AddWithValue("@Y", Y);
              else
                  command.Parameters.AddWithValue("@Y", System.DBNull.Value);

              if(active != null)
                  command.Parameters.AddWithValue("@active", active);
              else
                  command.Parameters.AddWithValue("@active", System.DBNull.Value);

              if(createdDate != null)
                  command.Parameters.AddWithValue("@createdDate", createdDate);
              else
                  command.Parameters.AddWithValue("@createdDate", System.DBNull.Value);

              if(createdBy != null)
                  command.Parameters.AddWithValue("@createdBy", createdBy);
              else
                  command.Parameters.AddWithValue("@createdBy", System.DBNull.Value);

              if(modifiedDate != null)
                  command.Parameters.AddWithValue("@modifiedDate", modifiedDate);
              else
                  command.Parameters.AddWithValue("@modifiedDate", System.DBNull.Value);

              if(modifiedBy != null)
                  command.Parameters.AddWithValue("@modifiedBy", modifiedBy);
              else
                  command.Parameters.AddWithValue("@modifiedBy", System.DBNull.Value);

         }

         /// <summary>
         /// Creates a Supply object from the passed data row
         /// </summary>
         private static Supply CreateSupplyFromDataRowShared(DataRow dr)
         {
             Supply objSupply = new Supply();

             objSupply.Id = (int)dr["Id"];

            if (dr["EmpID"] != System.DBNull.Value)
                objSupply.EmpID = (int)dr["EmpID"];
            else
                objSupply.EmpID = null;

            if (dr["RaisingID"] != System.DBNull.Value)
                 objSupply.RaisingID = (int)dr["RaisingID"];
             else
                 objSupply.RaisingID = null;

             if (dr["SchemeID"] != System.DBNull.Value)
                 objSupply.SchemeID = (int)dr["SchemeID"];
             else
                 objSupply.SchemeID = null;

             if (dr["CustomerID"] != System.DBNull.Value)
                 objSupply.CustomerID = (int)dr["CustomerID"];
             else
                 objSupply.CustomerID = null;

             if (dr["CustomerName"] != System.DBNull.Value)
                 objSupply.CustomerName = dr["CustomerName"].ToString();
             else
                 objSupply.CustomerName = null;

             if (dr["ArmedForcesID"] != System.DBNull.Value)
                 objSupply.ArmedForcesID = (int)dr["ArmedForcesID"];
             else
                 objSupply.ArmedForcesID = null;

             if (dr["Contact"] != System.DBNull.Value)
                 objSupply.Contact = dr["Contact"].ToString();
             else
                 objSupply.Contact = null;

             if (dr["Cnic"] != System.DBNull.Value)
                 objSupply.Cnic = dr["Cnic"].ToString();
             else
                 objSupply.Cnic = null;

             if (dr["SupplyDate"] != System.DBNull.Value)
                 objSupply.SupplyDate = (DateTime)dr["SupplyDate"];
             else
                 objSupply.SupplyDate = null;

             if (dr["X"] != System.DBNull.Value)
                 objSupply.X = (decimal)dr["X"];
             else
                 objSupply.X = null;

             if (dr["Y"] != System.DBNull.Value)
                 objSupply.Y = (decimal)dr["Y"];
             else
                 objSupply.Y = null;
             if (dr["Active"] != System.DBNull.Value)
                 objSupply.Active = (bool)dr["Active"];
             else
                 objSupply.Active = false;

             if (dr["CreatedDate"] != System.DBNull.Value)
                 objSupply.CreatedDate = (DateTime)dr["CreatedDate"];
             else
                 objSupply.CreatedDate = null;

             if (dr["CreatedBy"] != System.DBNull.Value)
                 objSupply.CreatedBy = (int)dr["CreatedBy"];
             else
                 objSupply.CreatedBy = null;

             if (dr["ModifiedDate"] != System.DBNull.Value)
                 objSupply.ModifiedDate = (DateTime)dr["ModifiedDate"];
             else
                 objSupply.ModifiedDate = null;

             if (dr["ModifiedBy"] != System.DBNull.Value)
                 objSupply.ModifiedBy = (int)dr["ModifiedBy"];
             else
                 objSupply.ModifiedBy = null;

             return objSupply;
         }

        private static Supply CreateSupplyFromDataRowSharedCustom(DataRow dr)
        {
            Supply objSupply = new Supply();

            objSupply.Id = (int)dr["Id"];

            if (dr["RaisingName"] != System.DBNull.Value)
                objSupply.RaisingName = (string)dr["RaisingName"];
            else
                objSupply.RaisingName = null;

            if (dr["CustomerName"] != System.DBNull.Value)
                objSupply.CustomerName = (string)dr["CustomerName"];
            else
                objSupply.CustomerName = null;

            if (dr["SchemeName"] != System.DBNull.Value)
                objSupply.SchemeName = (string)dr["SchemeName"];
            else
                objSupply.SchemeName = null;

            if (dr["TotalQty"] != System.DBNull.Value)
                objSupply.TotalQty = (int)dr["TotalQty"];
            else
                objSupply.TotalQty = null;

            if (dr["SuppliedDate"] != System.DBNull.Value)
                objSupply.SuppliedDate = (DateTime)dr["SuppliedDate"];
            else
                objSupply.SuppliedDate = null;

            return objSupply;
        }

    }
}
