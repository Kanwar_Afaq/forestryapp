using System; 
using System.Collections.Generic; 
using BusinessRepository.BusinessObject; 
 
namespace BusinessRepository.BusinessObject 
{ 
     /// <summary>
     /// This class inherits from the Generic List of Supply. 
     /// It's used so that you can iterate through the list or collection of the 
     /// Supply class.  You don't need to add any code to this helper class. 
     /// </summary>
     public class SupplyCollection : List<Supply> 
     { 
         // constructor 
         public SupplyCollection() 
         { 
         } 
     } 
} 
