﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessPOCO
{
    public class ResponseMessage
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsError { get; set; }
        public Object ResponseObject { get; set; }

        public ResponseMessage SuccessResponse(string message, Object obj = null)
        {
            this.Message = message;
            this.IsSuccess = true;
            this.IsError = false;
            this.ResponseObject = obj;

            return this;
        }

        public ResponseMessage UnSuccessResponse(string message)
        {
            this.Message = message;
            this.IsSuccess = false;
            this.IsError = false;

            return this;
        }

        public ResponseMessage ErrorResponse()
        {
            this.Message = "Something went wrong at server side.";
            this.IsSuccess = false;
            this.IsError = true;

            return this;
        }
    }
}
