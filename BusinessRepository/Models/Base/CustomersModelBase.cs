using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for CustomersModel.  Do not make changes to this class,
     /// instead, put additional code in the CustomersModel class 
     /// </summary>
     public class CustomersModelBase
     {
         public int Id { get; set; } 
        
         public string Name { get; set; } 
        
         public string Address { get; set; } 
        
         public string ContactNo { get; set; } 
        
         public string Cnic { get; set; } 
        
         public bool Active { get; set; } 
        
         public int? CreatedBy { get; set; } 
        
         public DateTime? CreatedDate { get; set; } 
        
         public int? ModifiedBy { get; set; } 
        
         public DateTime? ModifiedDate { get; set; } 

     }
}
