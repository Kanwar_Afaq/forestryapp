using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for EmployeeHierarchyModel.  Do not make changes to this class,
     /// instead, put additional code in the EmployeeHierarchyModel class 
     /// </summary>
     public class EmployeeHierarchyModelBase
     {
         public int Id { get; set; } 
        
         public int? ParentEmpID { get; set; } 
        
         public int? ChildEmpID { get; set; } 
        
         public int? CreatedBy { get; set; } 
        
         public DateTime? CreatedDate { get; set; } 
        
         public int? ModifiedBy { get; set; } 
        
         public DateTime? ModifiedDate { get; set; } 
        
         public bool Active { get; set; } 

     }
}
