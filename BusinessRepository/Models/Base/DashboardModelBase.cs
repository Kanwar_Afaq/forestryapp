﻿using BusinessRepository.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRepository.Models.Base
{
    public class DashboardModelBase
    {
        public int TotalPlantable { get; set; }

        public int TotalUnderSize { get; set; }

        public int TotalSupplied { get; set; }

        public List<Supply> MonthlySupplyList = new List<Supply>();

        public List<Employees> EmployeesList = new List<Employees>();

    }
}
