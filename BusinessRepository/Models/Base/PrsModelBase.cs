using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for PrsModel.  Do not make changes to this class,
     /// instead, put additional code in the PrsModel class 
     /// </summary>
     public class PrsModelBase
     {
         public int Id { get; set; } 
        
         public string PlantationType { get; set; } 
        
         public int? RaisingID { get; set; } 
        
         public string Name { get; set; } 
        
         public int? SupplyID { get; set; } 
        
         public string District { get; set; } 
        
         public string Taluka { get; set; } 
        
         public DateTime? PlantationDate { get; set; } 
        
         public decimal? Distance { get; set; } 
        
         public decimal DistanceTotal { get; set; } 
        
         public int? PlantsKm { get; set; } 
        
         public int? TotalPlants { get; set; } 
        
         public decimal? KMFrom { get; set; } 
        
         public decimal KMFromTotal { get; set; } 
        
         public decimal? KMTo { get; set; } 
        
         public decimal KMToTotal { get; set; } 
        
         public bool RestockingDone { get; set; } 
        
         public int? NoOfRestocking { get; set; } 
        
         public string SuccessPercent { get; set; } 
        
         public int? LocationID { get; set; } 
        
         public int? SchemeID { get; set; } 
        
         public DateTime? CreatedDate { get; set; } 

         public int? CreatedBy { get; set; } 
        
         public DateTime? ModifiedDate { get; set; } 
        
         public int? ModifiedBy { get; set; } 
        
         public bool Active { get; set; } 

     }
}
