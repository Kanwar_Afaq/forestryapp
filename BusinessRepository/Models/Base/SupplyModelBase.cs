using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for SupplyModel.  Do not make changes to this class,
     /// instead, put additional code in the SupplyModel class 
     /// </summary>
     public class SupplyModelBase
     {
         public int Id { get; set; }

        public int? EmpID { get; set; }

        public int? RaisingID { get; set; } 
        
         public int? SchemeID { get; set; } 
        
         public int? CustomerID { get; set; } 
        
         public string CustomerName { get; set; } 
        
         public int? ArmedForcesID { get; set; } 
        
         public string Contact { get; set; } 
        
         public string Cnic { get; set; } 
        
         public DateTime? SupplyDate { get; set; } 
        
         public decimal? X { get; set; } 
        
         public decimal XTotal { get; set; } 
        
         public decimal? Y { get; set; } 
        
         public decimal YTotal { get; set; } 
        
         public bool Active { get; set; } 
        
         public DateTime? CreatedDate { get; set; } 
        
         public int? CreatedBy { get; set; } 
        
         public DateTime? ModifiedDate { get; set; } 
        
         public int? ModifiedBy { get; set; } 

     }
}
