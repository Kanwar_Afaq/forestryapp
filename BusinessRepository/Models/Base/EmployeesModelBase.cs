using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for EmployeesModel.  Do not make changes to this class,
     /// instead, put additional code in the EmployeesModel class 
     /// </summary>
     public class EmployeesModelBase
     {
         public int Id { get; set; } 
        
         public string UserName { get; set; } 
        
         public string Password { get; set; } 
        
         public string FirstName { get; set; } 
        
         public string LastName { get; set; } 
        
         public int? DepartmentID { get; set; } 
        
         public int? DesignationID { get; set; } 
        
         public int? LocationID { get; set; } 
        
         public int? RoleID { get; set; } 
        
         public string Email { get; set; } 
        
         public string Address { get; set; } 
        
         public int? ModifiedBy { get; set; } 
        
         public DateTime? ModifiedDate { get; set; } 
        
         public bool Active { get; set; } 

     }
}
