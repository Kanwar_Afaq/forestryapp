using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for SupplyItemsModel.  Do not make changes to this class,
     /// instead, put additional code in the SupplyItemsModel class 
     /// </summary>
     public class SupplyItemsModelBase
     {
         public int Id { get; set; } 
        
         public int? SupplyID { get; set; } 
        
         public int? ItemID { get; set; } 
        
         public int? ItemSizeID { get; set; } 
        
         public int? TotalQty { get; set; } 
        
         public int? MicroQty { get; set; } 
        
         public int? MacroQty { get; set; } 
        
         public int? PottedQty { get; set; } 
        
         public bool Active { get; set; } 
        
         public DateTime? CreatedDate { get; set; } 
        
         public int? CreatedBy { get; set; } 
        
         public DateTime? ModifiedDate { get; set; } 
        
         public int? ModifiedBy { get; set; } 

     }
}
