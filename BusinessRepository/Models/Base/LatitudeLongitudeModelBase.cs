using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for LatitudeLongitudeModel.  Do not make changes to this class,
     /// instead, put additional code in the LatitudeLongitudeModel class 
     /// </summary>
     public class LatitudeLongitudeModelBase
     {
         public int Id { get; set; } 
        
         public int? SupplyID { get; set; } 
        
         public string PlantationName { get; set; } 
        
         public decimal? Latitude { get; set; } 
        
         public decimal LatitudeTotal { get; set; } 
        
         public decimal? Longitude { get; set; } 
        
         public decimal LongitudeTotal { get; set; } 

     }
}
