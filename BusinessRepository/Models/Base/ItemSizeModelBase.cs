using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for ItemSizeModel.  Do not make changes to this class,
     /// instead, put additional code in the ItemSizeModel class 
     /// </summary>
     public class ItemSizeModelBase
     {
         public int Id { get; set; } 
        
         public string Size { get; set; } 
        
         public bool IsUndersize { get; set; } 
        
         public bool Active { get; set; } 
        
         public DateTime? CreatedDate { get; set; } 
        
         public int? CreatedBy { get; set; } 
        
         public DateTime? ModifiedDate { get; set; } 
        
         public int? ModifiedBy { get; set; } 

     }
}
