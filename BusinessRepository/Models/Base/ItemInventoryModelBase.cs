using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for ItemInventoryModel.  Do not make changes to this class,
     /// instead, put additional code in the ItemInventoryModel class 
     /// </summary>
     public class ItemInventoryModelBase
     {
         public int Id { get; set; } 
        
         public int? ItemID { get; set; } 
        
         public int? EmpID { get; set; } 
        
         public int? ItemSizeID { get; set; } 
        
         public int? TotalQty { get; set; } 
        
         public int? MicroQty { get; set; } 
        
         public int? MacroQty { get; set; } 
        
         public int? PottedQty { get; set; } 
        
         public int? RaisingID { get; set; } 
        
         public int? SchemeID { get; set; } 
        
         public bool Active { get; set; } 
        
         public DateTime? CreatedDate { get; set; } 
        
         public int? CreatedBy { get; set; } 

     }
}
