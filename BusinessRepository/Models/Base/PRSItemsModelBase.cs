using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for PRSItemsModel.  Do not make changes to this class,
     /// instead, put additional code in the PRSItemsModel class 
     /// </summary>
     public class PRSItemsModelBase
     {
         public int Id { get; set; } 
        
         public int? Prsid { get; set; } 
        
         public int? ItemID { get; set; } 

     }
}
