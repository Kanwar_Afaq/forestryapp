using System;
using System.ComponentModel;

namespace BusinessRepository.Models.Base
{
     /// <summary>
     /// Base class for ItemsModel.  Do not make changes to this class,
     /// instead, put additional code in the ItemsModel class 
     /// </summary>
     public class ItemsModelBase
     {
         public int Id { get; set; } 
        
         public string Name { get; set; } 

         public string Description { get; set; } 
        
         public bool Active { get; set; } 
        
         public DateTime? CreatedDate { get; set; } 
        
         public int? CreatedBy { get; set; } 
        
         public DateTime? ModifiedDate { get; set; } 
        
         public int? ModifiedBy { get; set; } 

     }
}
