using System;
using System.Collections.Generic;
using BusinessRepository.Models.Base;

namespace BusinessRepository.Models
{ 
     /// <summary>
     /// This file will not be overwritten.  You can put
     /// additional LatitudeLongitude Model code in this class.
     /// </summary>
     public class LatitudeLongitudeModel : LatitudeLongitudeModelBase
     {
        public string EmpName { get; set; }

        public List<Decimal[]> LatLngArr = new List<Decimal[]>();
    } 
} 
