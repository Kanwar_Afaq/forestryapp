using System;
using BusinessRepository.Models.Base;

namespace BusinessRepository.Models
{ 
     /// <summary>
     /// This file will not be overwritten.  You can put
     /// additional Raising Model code in this class.
     /// </summary>
     public class RaisingModel : RaisingModelBase
     { 
     } 
} 
