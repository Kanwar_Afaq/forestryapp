using System;
using BusinessRepository.Models.Base;

namespace BusinessRepository.Models
{ 
     /// <summary>
     /// This file will not be overwritten.  You can put
     /// additional Roles Model code in this class.
     /// </summary>
     public class RolesModel : RolesModelBase
     { 
     } 
} 
